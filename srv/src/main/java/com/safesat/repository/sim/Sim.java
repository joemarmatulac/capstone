package com.safesat.repository.sim;

import com.safesat.repository.BaseDomain;
import com.safesat.service.sim.SimService;
import com.safesat.service.sim.SimServiceImpl;

import javax.persistence.*;

@Entity(name = "sim")
public class Sim extends BaseDomain {

	@Column(unique = true, length = 50)
	private String controlNo;
	private String simSerialNo;
	private String accountNo;
	@Column(unique = true, length = 50)
	private String mobileNo;
	private Long dateReceived;


	public Long getDateReceived() {
		return dateReceived;
	}

	public void setDateReceived(Long dateReceived) {
		this.dateReceived = dateReceived;
	}

	public Sim() {}

	public Sim(String controlNo, String simSerialNo, String accountNo, String mobileNo, Long dateReceived) {
		this.controlNo = controlNo;
		this.simSerialNo = simSerialNo;
		this.accountNo = accountNo;
		this.mobileNo = mobileNo;
		this.dateReceived = dateReceived;
	}

	public String getControlNo() {
        return controlNo;
	}

	public void setControlNo(String controlNo) {
		this.controlNo = controlNo;
	}

	public String getSimSerialNo() {
		return simSerialNo;
	}

	public void setSimSerialNo(String simSerialNo) {
		this.simSerialNo = simSerialNo;
	}


	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	@Override
	public String toString() {
		return "Sim [controlNo=" + controlNo + ", simSerialNo=" + simSerialNo + ", accountNo=" + accountNo
				+ ", mobileNo=" + mobileNo + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountNo == null) ? 0 : accountNo.hashCode());
		result = prime * result + ((controlNo == null) ? 0 : controlNo.hashCode());
		result = prime * result + ((mobileNo == null) ? 0 : mobileNo.hashCode());
		result = prime * result + ((simSerialNo == null) ? 0 : simSerialNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sim other = (Sim) obj;
		if (accountNo == null) {
			if (other.accountNo != null)
				return false;
		} else if (!accountNo.equals(other.accountNo))
			return false;
		if (controlNo == null) {
			if (other.controlNo != null)
				return false;
		} else if (!controlNo.equals(other.controlNo))
			return false;
		if (mobileNo == null) {
			if (other.mobileNo != null)
				return false;
		} else if (!mobileNo.equals(other.mobileNo))
			return false;
		if (simSerialNo == null) {
			if (other.simSerialNo != null)
				return false;
		} else if (!simSerialNo.equals(other.simSerialNo))
			return false;
		return true;
	}
    
    


    
}
