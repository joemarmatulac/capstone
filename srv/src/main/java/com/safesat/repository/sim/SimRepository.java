package com.safesat.repository.sim;

import java.util.List;

import com.safesat.repository.sim.Sim;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SimRepository extends JpaRepository<Sim, Long> {

	List<Sim> findByMobileNo(String mobileNo);
	List<Sim> findByAccountNo(String accountNo);
	List<Sim> findByControlNo(String controlNo);
	List<Sim> findBySimSerialNo(String serialNo);
	List<Sim> findByActive(boolean active);

}
