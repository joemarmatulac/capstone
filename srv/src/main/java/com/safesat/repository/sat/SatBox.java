package com.safesat.repository.sat;

import com.safesat.repository.BaseDomain;
import com.safesat.repository.sim.Sim;

import javax.persistence.*;

@Entity(name = "satBox")
public class SatBox extends BaseDomain {

	@Column(unique = true, length = 50)
	private String controlNo;
	@Column(unique = true, length = 50)
    private String imei;
    private String modelName;
	private String clientAccount;
	@Column(unique = true, length = 50)
    private String vehiclePlateNo;
    private Long dateIssued;
	private Long dateReceived;

	public Long getDateReceived() {
		return dateReceived;
	}

	public void setDateReceived(Long dateReceived) {
		this.dateReceived = dateReceived;
	}

	@OneToOne
    @JoinColumn(name = "id_Sim")
    private Sim sim;
    
    public SatBox() {}

	public SatBox(String controlNo, String imei, String modelName, String clientAccount, String vehiclePlateNo, Long dateIssued, Long dateReceived, Sim sim) {
		this.controlNo = controlNo;
		this.imei = imei;
		this.modelName = modelName;
		this.clientAccount = clientAccount;
		this.vehiclePlateNo = vehiclePlateNo;
		this.dateIssued = dateIssued;
		this.dateReceived = dateReceived;
		this.sim = sim;
	}

	public Sim getSim() {
		return sim;
	}

	public void setSim(Sim sim) {
		this.sim = sim;
	}

	public String getControlNo() {
		return controlNo;
	}

	public void setControlNo(String controlNo) {
		this.controlNo = controlNo;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getClientAccount() {
		return clientAccount;
	}

	public void setClientAccount(String clientAccount) {
		this.clientAccount = clientAccount;
	}

	public String getVehiclePlateNo() {
		return vehiclePlateNo;
	}

	public void setVehiclePlateNo(String vehiclePlateNo) {
		this.vehiclePlateNo = vehiclePlateNo;
	}

	public Long getDateIssued() {
		return dateIssued;
	}

	public void setDateIssued(Long dateIssued) {
		this.dateIssued = dateIssued;
	}

	@Override
	public String toString() {
		return "SatBox [controlNo=" + controlNo + ", imei=" + imei + ", modelName=" + modelName + ", clientAccount="
				+ clientAccount + ", vehiclePlateNo=" + vehiclePlateNo + ", dateIssued=" + dateIssued + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientAccount == null) ? 0 : clientAccount.hashCode());
		result = prime * result + ((controlNo == null) ? 0 : controlNo.hashCode());
		result = prime * result + ((dateIssued == null) ? 0 : dateIssued.hashCode());
		result = prime * result + ((imei == null) ? 0 : imei.hashCode());
		result = prime * result + ((modelName == null) ? 0 : modelName.hashCode());
		result = prime * result + ((vehiclePlateNo == null) ? 0 : vehiclePlateNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SatBox other = (SatBox) obj;
		if (clientAccount == null) {
			if (other.clientAccount != null)
				return false;
		} else if (!clientAccount.equals(other.clientAccount))
			return false;
		if (controlNo == null) {
			if (other.controlNo != null)
				return false;
		} else if (!controlNo.equals(other.controlNo))
			return false;
		if (dateIssued != other.dateIssued)
			return false;
		if (imei == null) {
			if (other.imei != null)
				return false;
		} else if (!imei.equals(other.imei))
			return false;
		if (modelName == null) {
			if (other.modelName != null)
				return false;
		} else if (!modelName.equals(other.modelName))
			return false;
		if (vehiclePlateNo == null) {
			if (other.vehiclePlateNo != null)
				return false;
		} else if (!vehiclePlateNo.equals(other.vehiclePlateNo))
			return false;
		return true;
	}

    
}
