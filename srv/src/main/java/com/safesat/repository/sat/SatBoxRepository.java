package com.safesat.repository.sat;

import java.util.List;

import com.safesat.repository.sat.SatBox;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SatBoxRepository extends JpaRepository<SatBox, Long> {
	
	List<SatBox> findBySimSimSerialNo(String simSerialNo);
	List<SatBox> findBySimMobileNo(String mobileNo);
	List<SatBox> findBySimAccountNo(String accountNo);
	List<SatBox> findByControlNo(String controlNo);
	List<SatBox> findByImei(String Imei);
	List<SatBox> findByVehiclePlateNo(String vehiclePlateNo);
	List<SatBox> findByActive(boolean active);

}
