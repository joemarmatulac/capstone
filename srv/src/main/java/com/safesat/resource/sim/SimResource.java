package com.safesat.resource.sim;

import com.safesat.exception.DuplicateException;
import com.safesat.repository.sim.Sim;
import com.safesat.service.sim.SimService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/sim")
public class SimResource {
    @Autowired
    private SimService simService;

    @PostMapping("/add")
    public Sim add(@RequestBody Sim sim) {
    	return simService.add(sim); }

	@PostMapping("/addAll")
	public List<Sim> addAll(@RequestBody List<Sim> sims) {
		return simService.addAll(sims); }

    @GetMapping("/all")
    public List<Sim> findAll() {
        return simService.findAll();
    }

    @PostMapping("/validate")
    public boolean validate(@RequestBody Sim sim) throws DuplicateException{
		return simService.validate(sim);
	}

    @GetMapping("/generateControlNo")
    public String generateControlNo() {
        return simService.generateControlNo();
    }

	@PostMapping ("/update")
	public Sim update(@RequestBody Sim sim){
		return simService.update(sim);
	}

	@GetMapping("/active")
	public List<Sim> findByActive() {
		return simService.findByActive(true);	}

	@PutMapping("/changeStatus/{id}")
	public Sim changeStatus(@PathVariable("id") long id){
		return simService.changeStatus(id);   
	}

	@GetMapping("/findMobileNo/{mobileNo}")
	public List<Sim> findByMobileNo(@PathVariable("mobileNo") String mobileNo) {
		return simService.findByMobileNo(mobileNo);	}
	
	@GetMapping("/findAccountNo/{accountNo}")
	public List<Sim> findByAccountNo(@PathVariable("accountNo") String accountNo) {
		return simService.findByAccountNo(accountNo);	}
}
