package com.safesat.resource.sat;

import com.safesat.exception.DuplicateException;
import com.safesat.repository.sat.SatBox;
import com.safesat.repository.sim.Sim;
import com.safesat.service.sat.SatBoxService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/satBox")
public class SatBoxResource {
    @Autowired
    private SatBoxService satBoxService;

    @PostMapping( "/add")
    public SatBox add(@RequestBody  SatBox satBox) {
        return satBoxService.add(satBox);
    }

	@PostMapping( "/addAll")
	public List<SatBox> addAll(@RequestBody  List<SatBox> sats) {
		return satBoxService.addAll(sats);
	}
    
    @GetMapping("/all")
    public List<SatBox> findAll() {
        return satBoxService.findAll();
    }

	@PostMapping("/validate")
	public boolean validate(@RequestBody SatBox sat) throws DuplicateException {
		return satBoxService.validate(sat);
	}

	@GetMapping("/generateControlNo")
	public String generateControlNo() {
		return satBoxService.generateControlNo();
	}
    
	@PostMapping("/update")
	public SatBox update(@RequestBody SatBox satBox){
		return satBoxService.update(satBox);
	}

	@GetMapping("/active")
	public List<SatBox> findByActive() {
		return satBoxService.findByActive(true);	}
	
	@PutMapping("/changeStatus/{id}")
	public SatBox changeStatus(@PathVariable("id") long id){
		return satBoxService.changeStatus(id);   
	}
	
	@GetMapping("/findSerialNo/{serialNo}")
	public List<SatBox> findSimSerialNo(@PathVariable("serialNo") String simSerialNo){
		 return satBoxService.findBySimSimSerialNo(simSerialNo);   
	}
	
	@GetMapping("/findMobileNo/{mobileNo}")
	public List<SatBox> findBySimMobileNo(@PathVariable("mobileNo") String mobileNo) {
		return satBoxService.findBySimMobileNo(mobileNo);
	}
	
	@GetMapping("/findAccountNo/{accountNo}")
	public List<SatBox> findByAccountNo(@PathVariable("accountNo") String accountNo) {
		return satBoxService.findBySimAccountNo(accountNo);
	}	
}
