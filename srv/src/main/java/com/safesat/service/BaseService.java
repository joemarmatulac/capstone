package com.safesat.service;

import com.safesat.exception.DuplicateException;
import com.safesat.repository.BaseDomain;

import java.util.List;

public interface BaseService<T extends BaseDomain> {
    T add(T t);
    List<T> addAll(List<T> t);
    List<T> findAll();
    T update(T t);
    T changeStatus(long id);
}
