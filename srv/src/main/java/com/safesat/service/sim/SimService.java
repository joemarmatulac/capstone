package com.safesat.service.sim;

import java.util.List;

import com.safesat.exception.DuplicateException;
import com.safesat.repository.sim.Sim;
import com.safesat.service.BaseService;

public interface SimService extends BaseService<Sim> {

	List<Sim> findByMobileNo(String mobileNo);
	List<Sim> findByAccountNo(String accountNo);
	List<Sim> findByControlNo(String controlNo);
	List<Sim> findBySimSerialNo(String serialNo);
	List<Sim> findByActive(boolean active);
	String generateControlNo();
	boolean validate(Sim sim) throws DuplicateException;
}
