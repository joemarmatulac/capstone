package com.safesat.service.sim;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.safesat.exception.DuplicateException;
import com.safesat.service.sim.SimService;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.safesat.repository.sim.Sim;
import com.safesat.repository.sim.SimRepository;
import org.springframework.web.bind.annotation.ResponseStatus;

@Service
public class SimServiceImpl implements SimService {
    @Autowired private SimRepository simRepository;

    @Override
    public Sim add(Sim sim){
		return simRepository.save(sim);
	}

	@Override
	public List<Sim> addAll(List<Sim> sims){
		return simRepository.saveAll(sims);
	}

	@Override
	public List<Sim> findAll() {
		return simRepository.findAll();
	}
    
	@Override
	public Sim update(Sim sim) {
    	return simRepository.save(sim);
	}

	@Override
	public Sim changeStatus(long id) {
		Optional<Sim> sim= simRepository.findById(id);
		if (sim.isPresent()){
			Sim simFound = sim.get();
			simFound.setActive(!(simFound.isActive()));
			return simRepository.save(simFound);
		}
		return null;
    }
	
	@Override
	public List<Sim> findByMobileNo(String mobileNo) {
		return simRepository.findByMobileNo(mobileNo);
	}

	@Override
	public List<Sim> findByAccountNo(String accountNo) {
		return simRepository.findByAccountNo(accountNo);
		}

	@Override
	public List<Sim> findByControlNo(String controlNo) {
		return simRepository.findByControlNo(controlNo);
	}

	@Override
	public List<Sim> findBySimSerialNo(String serialNo) { return simRepository.findBySimSerialNo(serialNo); }

	@Override
	public List<Sim> findByActive(boolean active) {
		return simRepository.findByActive(active);
	}


	@Override
	public String generateControlNo() {
		int counter = simRepository.findAll().size() + 1;
		String date = new SimpleDateFormat("ddMMyyy").format(new Date());
		return  "G-" + counter + "-" + date;
	}

	@Override
	public boolean validate(Sim sim) throws DuplicateException{
    	List<Sim> sims = simRepository.findByControlNo(sim.getControlNo());
    	if (!sims.isEmpty() && !sims.get(0).getId().equals(sim.getId())){
		    throw new DuplicateException("Control number is already taken! Please enter unique one!");
    	}
		sims = simRepository.findBySimSerialNo(sim.getSimSerialNo());
    	if (sim.getSimSerialNo()!= null){
		  if (!sims.isEmpty() && !sims.get(0).getId().equals(sim.getId())){
			throw new DuplicateException("Sim serial number is already taken! Please enter unique one!");
		}}
//		sims = simRepository.findByAccountNo(sim.getAccountNo());
//		if (!sims.isEmpty() && !sims.get(0).getId().equals(sim.getId())){
//			throw new DuplicateException("Account Number is already taken! Please enter unique one!");
//		}
		sims = simRepository.findByMobileNo(sim.getMobileNo());
		if (!sims.isEmpty() && !sims.get(0).getId().equals(sim.getId())){
			throw new DuplicateException("Mobile Number is already taken! Please enter unique one!");
		}
    	return true;
	}

}
