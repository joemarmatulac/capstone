package com.safesat.service.sat;

import java.util.List;

import com.safesat.exception.DuplicateException;
import com.safesat.repository.sat.SatBox;
import com.safesat.repository.sim.Sim;
import com.safesat.service.BaseService;


public interface SatBoxService extends BaseService<SatBox> {
	
	List<SatBox> findBySimSimSerialNo(String simSerialNo);
	List<SatBox> findBySimMobileNo(String mobileNo);
	List<SatBox> findBySimAccountNo(String accountNo);
	List<SatBox> findByControlNo(String controlNo);
	List<SatBox> findByImei(String Imei);
	List<SatBox> findByVehiclePlateNo(String vehiclePlateNo);
	List<SatBox> findByActive(boolean active);
	String generateControlNo();
	boolean validate(SatBox sat) throws DuplicateException;

}
