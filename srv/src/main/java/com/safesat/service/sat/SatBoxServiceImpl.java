package com.safesat.service.sat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.safesat.exception.DuplicateException;
import com.safesat.repository.sim.Sim;
import com.safesat.service.sat.SatBoxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.safesat.repository.sat.SatBox;
import com.safesat.repository.sat.SatBoxRepository;

@Service
public class SatBoxServiceImpl implements SatBoxService {
    @Autowired private SatBoxRepository satBoxRepository;

    @Override
    public SatBox add(SatBox satBox) {
    	return satBoxRepository.save(satBox);
	}

	@Override
	public List<SatBox> addAll(List<SatBox> sats){
		return satBoxRepository.saveAll(sats);
	}

	@Override
	public List<SatBox> findAll() {
		return satBoxRepository.findAll();
	}
    
	@Override
	public SatBox update(SatBox satBox) {
		return satBoxRepository.save(satBox);
	}

	@Override
	public SatBox changeStatus(long id) {
		Optional<SatBox> satBox= satBoxRepository.findById(id);
		if (satBox.isPresent()){
			satBox.get().setActive(!(satBox.get().isActive()));
			return satBoxRepository.save(satBox.get());
		}
		return null;
	}



	@Override
	public List<SatBox> findBySimSimSerialNo(String simSerialNo) {
		return satBoxRepository.findBySimSimSerialNo(simSerialNo);
	}

	@Override
	public List<SatBox> findBySimMobileNo(String mobileNo) {
    	return satBoxRepository.findBySimMobileNo(mobileNo);
	}

	@Override
	public List<SatBox> findBySimAccountNo(String accountNo) {
    	return satBoxRepository.findBySimAccountNo(accountNo);
    }

	@Override
	public List<SatBox> findByControlNo(String controlNo) {
		return satBoxRepository.findByControlNo(controlNo);
	}

	@Override
	public List<SatBox> findByImei(String imei) {
		return satBoxRepository.findByImei(imei);
	}

	@Override
	public List<SatBox> findByActive(boolean active) {
		return satBoxRepository.findByActive(active);
	}

	@Override
	public List<SatBox> findByVehiclePlateNo(String vehiclePlateNo) {
		return satBoxRepository.findByVehiclePlateNo(vehiclePlateNo);
	}

	@Override
	public String generateControlNo() {
		int counter = satBoxRepository.findAll().size() + 1;
		String date = new SimpleDateFormat("ddMMyyy").format(new Date());
		return "T9-" + counter + "-" + date;
	}

	@Override
	public boolean validate(SatBox sat) throws DuplicateException {
		List<SatBox> sats = satBoxRepository.findByControlNo(sat.getControlNo());
		if (!sats.isEmpty() && !sats.get(0).getId().equals(sat.getId())){
			throw new DuplicateException("Control number is already taken! Please enter unique one!");
		}
		sats = satBoxRepository.findByImei(sat.getImei());
		if (!sats.isEmpty() && !sats.get(0).getId().equals(sat.getId())){
			throw new DuplicateException("Imei is already taken! Please enter unique one!");
		}
		sats = satBoxRepository.findByVehiclePlateNo(sat.getVehiclePlateNo());
		if (sat.getVehiclePlateNo()!= null){
		  if (!sats.isEmpty() && !sats.get(0).getId().equals(sat.getId())){
			throw new DuplicateException("Vehicle plate number is already taken! Please enter unique one!");
		}}
		return true;
	}

}
