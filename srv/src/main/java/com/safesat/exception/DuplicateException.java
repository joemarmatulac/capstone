package com.safesat.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
public class DuplicateException extends Exception {

        public DuplicateException(String string)
        {
            super(string);
        }
}
