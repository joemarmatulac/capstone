package com.safesat.singleton;

public class Zoo {

	public static void main(String[] args) {
		System.out.println("Hello visitors, As you already know we sell only 15 tickets per day. \n");
		for (int i = 1; i <= 15; i++) {
			
			System.out.println("This is visitor number " + i + ", Please scan the ticket here and pass the gate number " + ZooEntranceDoor.getZooEntranceDoor().hashCode());
			System.out.println("Next Please!" + "\n");
		}	
		
		System.out.println("Ohhhh, 15 visitors just entered! Close the gate!" + "\n");

	}

}
