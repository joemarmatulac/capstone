package com.safesat.singleton;

public class ZooEntranceDoor {
	
	private static ZooEntranceDoor zooEntranceDoor = new ZooEntranceDoor();
	
	private ZooEntranceDoor() {}
	
	public static ZooEntranceDoor getZooEntranceDoor() {	
		
		return zooEntranceDoor;
	}
	
}
