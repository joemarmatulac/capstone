package com.safesat.templateMethod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DesktopReplacement extends QosmioX70 {

	@Override
	public void addMemory() {
		MemorySize memory = MemorySize.DDR3_8GB;
		System.out.println("Adding the "+ memory.toString() + " memory\n");
	}

	@Override
	public void addHardDiskDrive() {
		HardDiskDriveType hardDisk = HardDiskDriveType.HDD_1TB;
		System.out.println("Adding the " + hardDisk.toString() + " hard disk drive\n");
	}

	@Override
	public void addBlurayDiskBurner() {
		System.out.println("Adding the bluray \n");
	}
	
	@Override
	public boolean customerWantsBluray() {
		
		String answer = getUserInput();
		if (answer.toLowerCase().startsWith("y")) {
			return true;			
		} else {
			return false;
		}
		
	}	

	private String getUserInput() {
		
		String answer = null;
		System.out.println("Would you like to have Blu-ray disk burner, also (y/n)?");
		BufferedReader in= new BufferedReader(new InputStreamReader(System.in));
		try {
			answer = in.readLine();
		} catch (IOException io) {
			System.out.println("IO error when trying to read your answer!\n");
		}
		if (answer == null) {
			return "no";
		}
		return answer;
	}

	@Override
	public void addKeyboard() {
		KeyboardLanguage keyboard = KeyboardLanguage.AMERICAN;
		System.out.println("Adding the " + keyboard.toString() + " keyboard\n");
	}

	@Override
	public void addBacklightKeyboard() {
		System.out.println("Adding the backlight for keyboard \n");
	}

	@Override
	public boolean customerWantsBacklightKeyboard() {
		
		String answer = getUserInput1();
		if (answer.toLowerCase().startsWith("y")) {
			return true;			
		} else {
			return false;
		}
		
	}	

	private String getUserInput1() {
		
		String answer = null;
		System.out.println("Would you like to have backlight for your keyboard, also (y/n)?");
		BufferedReader in= new BufferedReader(new InputStreamReader(System.in));
		try {
			answer = in.readLine();
		} catch (IOException io) {
			System.out.println("IO error when trying to read your answer!\n");
		}
		if (answer == null) {
			return "no";
		}
		return answer;
	}
	
	@Override
	public void addLCD() {
		
		LCDDimension lcd = LCDDimension.INCH15_6;
		System.out.println("Adding the " + lcd.toString() + " screen\n" );
		
	}

}
