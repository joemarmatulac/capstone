package com.safesat.templateMethod;

public class LaptopBuilding {

	public static void main(String[] args) {

		DesktopReplacement desktop = new DesktopReplacement();
		System.out.println("Making Tecra Z50(a desktop replacement laptop)...\n");
		desktop.buildingLaptop();
		
		Lightweight lightwight = new Lightweight();
		System.out.println("Making Portege A30(a lightweight laptop)...\n");
		lightwight.buildingLaptop();
		
	}

}
