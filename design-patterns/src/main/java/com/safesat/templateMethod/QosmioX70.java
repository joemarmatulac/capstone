package com.safesat.templateMethod;

public abstract class QosmioX70 {
	enum MemorySize{DDR3_4GB, DDR3_6GB, DDR3_8GB}
	enum HardDiskDriveType{HDD_500GB, HDD_1TB, SSD_128GB, SSD_256GB}
	enum KeyboardLanguage{AMERICAN, EUROPEAN, ASIAN}
	enum LCDDimension{INCH13_3, INCH15_6, INCH17_3}

	final void buildingLaptop() {
		
		addChassis();
		addBoard();
		addMemory();
		addHardDiskDrive();
		if (customerWantsBluray()) {			
			addBlurayDiskBurner();
		}
		addKeyboard();
		if (customerWantsBacklightKeyboard()) {
			addBacklightKeyboard();
		}		
		addLCD();
		packagingAndInsertingManual();		
		
	}
	
	public void addChassis() {
		
		System.out.println("Adding the chassis \n");
		
	}
	
	public void addBoard() {
		
		System.out.println("Adding the board \n");

	}
	
	public abstract void addMemory();

	public abstract void addHardDiskDrive();
	
	public abstract void addBlurayDiskBurner();
	
	public abstract void addKeyboard();
	
	public abstract void addBacklightKeyboard();
	
	public abstract void addLCD();
	
	public void packagingAndInsertingManual() {
		
		System.out.println("Packaging and inserting the manual! \n");

	}
	
	boolean customerWantsBluray() {
		return true;
	}
	
	boolean customerWantsBacklightKeyboard() {
		return true;
	}

}


