package com.safesat.observer;

public interface Hotel {

	void addMember(Members m);
	void removeMember(Members m);
	void notifyMembers();
	
}
