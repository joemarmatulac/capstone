package com.safesat.observer;
import java.util.ArrayList;

public class BlueSuit implements Hotel {

	private ArrayList<Members> members; 
	
	private boolean onSale;
	
	public BlueSuit() {
		
		members = new ArrayList<Members>();
		
	}
	
	public boolean isOnSale() {
		return onSale;
	}

	public void setOnSale(boolean onSale) {
		this.onSale = onSale;
		notifyMembers();
	}
	
	
	@Override
	public void addMember(Members m) {

		members.add(m);

	}

	@Override
	public void removeMember(Members m) {

		int memberIndex = members.indexOf(m);
		System.out.println("\nMember " + (memberIndex+1) + " is just deleted! :( ");
		members.remove(memberIndex);
		
	}

	@Override
	public void notifyMembers() {
		
		if (isOnSale()) {
			
			System.out.println("\nSale status has changed! Sale is started now! :D So notifying all the members! ");
		}
		
		else {
			
			System.out.println("\nUnfortunately, Sale status has changed! there is no sale now. Soon, we will have another sale! ");
		}		

		for (Members member: members) {
			member.update();
		}
		
	}



}
