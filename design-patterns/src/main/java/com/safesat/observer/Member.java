package com.safesat.observer;

public class Member implements Members {
	
	private static int numberId;
	private int number;
	
	public Member() {

		this.number = ++numberId;
		System.out.println("We have a new member: Member" + this.number + "!");
	}
	
	@Override
	public void update() {

		bookRoom();	
	}

	private void bookRoom() {
			
			System.out.println("Lets go!");		

	}

}
