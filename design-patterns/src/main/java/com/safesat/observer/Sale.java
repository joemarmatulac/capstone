package com.safesat.observer;

public class Sale {


	public static void main(String[] args) {

		BlueSuit blueSuit = new BlueSuit();
		
		Member maryam = new Member();
		blueSuit.addMember(maryam);

		Member amin = new Member();
		blueSuit.addMember(amin);

		Member mina = new Member();		
		blueSuit.addMember(mina);
		
		System.out.println("\n BlueSuit current sale is: \n" + blueSuit.isOnSale());
		
		blueSuit.setOnSale(true);

		blueSuit.removeMember(mina);
		
		blueSuit.setOnSale(false);
		
		System.out.println();
		Member mi = new Member();
		blueSuit.addMember(mi);

		Member min = new Member();
		blueSuit.addMember(min);

		
		blueSuit.setOnSale(true);
		
		
	}

}
