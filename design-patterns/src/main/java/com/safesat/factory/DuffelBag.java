package com.safesat.factory;

public class DuffelBag extends Bag {

	@Override
	public String getDescreption() {
		return "DuffelBag";
	}

	@Override
	public int getCost() {
		return 60;
	}

}
