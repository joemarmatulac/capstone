package com.safesat.factory;

public class MessengerBag extends Bag {

	@Override
	public String getDescreption() {
		return "MessengerBag";
	}

	@Override
	public int getCost() {
		return 30;
	}

	
	
}
