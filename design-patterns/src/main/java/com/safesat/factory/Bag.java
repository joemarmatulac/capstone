package com.safesat.factory;

public abstract class Bag {
	
	public abstract String getDescreption();
	public abstract int getCost();

}
