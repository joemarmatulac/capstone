package com.safesat.factory;

public class FactoryMain {

	public static void main(String[] args) {

		String bagName = "Backpack";
		Bag bag = BagFactory.createBag(bagName);
		System.out.println("The bag that you asked for that is created!");
		System.out.println("Type of the bag is: " + bag.getDescreption());
		System.out.println("And price of the bag is: " + bag.getCost());
		
		String bagName1 = "Laptop";
		Bag bag1 = BagFactory.createBag(bagName1);
		System.out.println("The bag that you asked for that is created!");
		System.out.println("Type of the bag is: " + bag1.getDescreption());
		System.out.println("And price of the bag is: " + bag1.getCost());
		
	}

}
