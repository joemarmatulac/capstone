package com.safesat.factory;

public class BackpackBag extends Bag {

	@Override
	public String getDescreption() {
		return "BackpackBag";
	}

	@Override
	public int getCost() {
		return 50;
	}
	

}
