package com.safesat.factory;

public class BagFactory {
	
	public static Bag createBag(String bagName) {
		
		Bag bag = null;
		
		if (bagName.equals("Backpack")) {
			bag = new BackpackBag();
		} 
		
		else if (bagName.equals("Messenger")) {
			bag = new MessengerBag();
		} 
		
		else if (bagName.equals("Duffel")) {
			bag = new DuffelBag();
		} 
		
		else if (bagName.equals("Laptop")) {
			bag = new LaptopBag();
		}
		
		return bag;		
	}

}
