package com.safesat.factory;

public class LaptopBag extends Bag {

	@Override
	public String getDescreption() {
		return "LaptopBag";
	}

	@Override
	public int getCost() {
		return 35;
	}

}
