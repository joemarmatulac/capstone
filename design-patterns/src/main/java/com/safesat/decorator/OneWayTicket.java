package com.safesat.decorator;

public class OneWayTicket implements AirPlaneTicket {
	
	@Override
	public String getDescription() {

		return "This is Economy Plane Ticket";

	}

	@Override
	public double getPrice() {

		return 120;

	}

}
