package com.safesat.decorator;

public class Food extends AddsOnDecorator{

	public Food(AirPlaneTicket myTicket) {
		
		super(myTicket);
		
		System.out.println("Adding Food");

	}
	
	@Override
	public String getDescription() {
		
		return myTicket.getDescription() + " + Food";
		
	}
	
	@Override
	public double getPrice() {

		return myTicket.getPrice() + 8;
		
	}

}
