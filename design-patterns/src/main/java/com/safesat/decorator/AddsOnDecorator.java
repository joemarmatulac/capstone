package com.safesat.decorator;

public abstract class AddsOnDecorator implements AirPlaneTicket{
 
	AirPlaneTicket myTicket;

	public AddsOnDecorator(AirPlaneTicket myTicket) {

		this.myTicket = myTicket;
	}	

}
