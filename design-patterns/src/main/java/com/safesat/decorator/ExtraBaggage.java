package com.safesat.decorator;

public class ExtraBaggage extends AddsOnDecorator {

	public ExtraBaggage(AirPlaneTicket myTicket) {
		
		super(myTicket);

		System.out.println("Adding ExtraBaggage");
	
	}

	@Override
	public String getDescription() {
		
		return myTicket.getDescription() + " + ExtraBaggage";
		
	}
	
	@Override
	public double getPrice() {

		return myTicket.getPrice() + 45;
		
	}
	
}
