package com.safesat.decorator;

public class Seat extends AddsOnDecorator{

	public Seat(AirPlaneTicket myTicket) {
		
		super(myTicket);
		
		System.out.println("Adding Seat");

	}
	
	@Override
	public String getDescription() {
		
		return myTicket.getDescription() + " + Seat";
		
	}
	
	@Override
	public double getPrice() {

		return myTicket.getPrice() + 8;
		
	}

}
