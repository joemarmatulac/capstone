package com.safesat.decorator;

public class Agency {

	public static void main(String[] args) {

		AirPlaneTicket oneWayTicket = new OneWayTicket();
		oneWayTicket = new Seat(oneWayTicket);
		oneWayTicket = new Food(oneWayTicket);
		oneWayTicket = new ExtraBaggage(oneWayTicket);
		

		System.out.println("Ticket's Description: " + oneWayTicket.getDescription());
		System.out.println("Ticket's Price: " + oneWayTicket.getPrice());
		
		
	}

}
