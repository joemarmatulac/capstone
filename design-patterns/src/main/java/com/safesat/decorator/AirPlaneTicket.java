package com.safesat.decorator;

public interface AirPlaneTicket {
	
	String getDescription();
	double getPrice();
	
}
