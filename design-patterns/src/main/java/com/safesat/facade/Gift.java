package com.safesat.facade;

public class Gift {

	private boolean giftOrdered;

	public boolean isGiftOrdered() {
		return giftOrdered;
	}

	public void setGiftOrdered(boolean giftOrdered) {
		this.giftOrdered = giftOrdered;
		
		if (giftOrdered) {
			System.out.print("Gift has already been ordered! \n");
		}
		else {
			System.out.print("You have to buy a gift! \n");

		}

	}

	
}
