package com.safesat.facade;

public class Restaurant {

	private boolean restaurantReserved;

	public boolean isRestaurantReserved() {
		
		return restaurantReserved;
	}

	public void setRestaurantReserved(boolean restaurantReserved) {
		
		this.restaurantReserved = restaurantReserved;
		if (this.restaurantReserved) {
			System.out.print("Restaurant is already reserved! \n");
		}
		else {
			System.out.print("You have to reserve a restaurant! \n");

		}

	}

	
}
