package com.safesat.facade;

public class BirthdayCake {
	
	private boolean cakeOrdered = false;

	public boolean isCakeOrdered() {
		return cakeOrdered;
	}

	public void setCakeOrdered(boolean cakeOrdered) {
		
		this.cakeOrdered = cakeOrdered;
		
		if (this.cakeOrdered) {
			System.out.print("Cake is already ordered! \n");	
		}
		else {
			System.out.print("You have to buy a cake! \n");

		}
	
	}
	
	

}
