package com.safesat.facade;

public class BirthdayFacade{

	
	public void start() {

		Welcome welcome = new Welcome();
		
		inviteFriends("Maryam");
		inviteFriends("Amin");
		inviteFriends("Sara");
		inviteFriends("Mina");

		Gift gift = new Gift();
		gift.setGiftOrdered(true);
		
		BirthdayCake birthdayCake = new BirthdayCake();
		birthdayCake.setCakeOrdered(true);
		
		Restaurant restaurant = new Restaurant();	
		restaurant.setRestaurantReserved(false);
		
		if (birthdayCake.isCakeOrdered()^ gift.isGiftOrdered()^ restaurant.isRestaurantReserved()) {
		System.out.println("Wowwww, Everything is done! HappyBirthday my frienddd! :D \n");
		}
		else {
			System.out.println("Attention! You still have something to do! \n");
			
		}
	}

	private void inviteFriends(String name) {
		new Friends(name);
		System.out.println("I've just invited " + name + "! Please remember to don't tell her anything about this :D \n");
	}
	
}
