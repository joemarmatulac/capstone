package com.safesat.presenter;

import com.safesat.commons.PropertiesFileReader;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main extends Application{

	private final Logger log = LoggerFactory.getLogger(Main.class);

	@Override
	public void start(Stage primaryStage) {
		try {
			PropertiesFileReader.INSTANCE.loadProperties("config.properties");
			Parent root = FXMLLoader.load(getClass().getResource("/fxml/main.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("/css/style.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setMaximized(true);
			primaryStage.show();
		} catch(Exception e) {
			log.error("Couldn't load the home FXML", e);
		}
	}


	public static void main(String[] args) {
		launch(args);
	}

}
