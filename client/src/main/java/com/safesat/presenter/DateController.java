package com.safesat.presenter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;

public class DateController {

    public Long datePickerToLong(LocalDate value){
        if (null == value) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.set(value.getYear(), value.getMonthValue()-1, value.getDayOfMonth());
        return c.getTime().getTime();
    }

    public LocalDate longToDatePicker(Long var){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(var);
        return LocalDate.parse(dateFormat.format(calendar.getTime()));
    }

    public String longToString(Long var){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(var);
        return (LocalDate.parse(dateFormat.format(calendar.getTime())).toString());
    }
}
