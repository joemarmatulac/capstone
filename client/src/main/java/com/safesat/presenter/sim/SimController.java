package com.safesat.presenter.sim;

import com.safesat.businesslogic.sim.SimBusinessLogic;
import com.safesat.commons.CapstoneException;
import com.safesat.commons.Sim;
import com.safesat.presenter.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


public class SimController {

    @FXML
    private Button btn_SaveSim;

    @FXML
    private Button btn_CancelSim;

    @FXML
    private Button btn_SaveUpdatedSim;

    @FXML
    private Button btn_ConfirmDeleteSim;

    @FXML
    private Button btn_CancelDeleteSim;

    @FXML
    private TextField txt_ControlNumberSim;

    @FXML
    private TextField txt_SimSerialNumberSim;

    @FXML
    private TextField txt_AccountNumberSim;

    @FXML
    private TextField txt_MobileNumberSim;

    @FXML
    private TextField txt_RemarksSim;

    @FXML
    private Label lbl_DeleteSim;

    @FXML
    private Label lbl_ControlNumberSim;

    @FXML
    private Label lbl_AccountNumberSim;

    @FXML
    private Label lbl_MobileNumberSim;

    @FXML
    private Label lbl_WelcomeSim;

    @FXML
    private Label lbl_InformStatusSim;

    @FXML
    private DatePicker datePickerSim;

    @FXML
    private Label lbl_DatePickerSim;

    private Sim simSelected;

    private DateController dateController;

    private SimBusinessLogic simBusinessLogic;

    private MainFxmlLoader mainFxmlLoader;

    private final Logger log = LoggerFactory.getLogger(SimController.class);


    private MainFxmlLoader getMainFxmlLoader(){
        if (mainFxmlLoader == null){
            mainFxmlLoader= new MainFxmlLoader();
        }return mainFxmlLoader;
    }

    private DateController dateHandlerSim(){
        if (dateController == null){
            dateController= new DateController();
        }return  dateController;
    }

    private SimBusinessLogic getLogicSim() {
        if (simBusinessLogic == null) {
            simBusinessLogic = new SimBusinessLogic(this);
        }return simBusinessLogic;
    }

    public Sim getSimSelected() {
        if (simSelected == null) {
            simSelected = new Sim();
        }return simSelected;
    }

    public void setSimSelected(Sim simSelected) {
        if (simSelected.getId() == null){
            initializeCreateViewSim();
        }else{
            this.simSelected = simSelected;
            initializeUpdateViewSim();}
    }

    @FXML
    public void saveSim(ActionEvent event) {
        lbl_InformStatusSim.setText("");
        if ((checkTextFieldIsNotNullSim())) {
            setTextFieldValueInSim();
            saveSimOperations(event);
        }
    }


    @FXML
    void updateSim(ActionEvent event) {
        lbl_InformStatusSim.setText("");
        if (checkTextFieldIsNotNullSim()) {
            setTextFieldValueUpdateInSim();
            updateSimOperation(event);
        }
    }

    @FXML
    void deleteSim(ActionEvent event) {
        getLogicSim().changeStatus(getSimSelected().getId());
        changeDeleteControlsForConfirmingDeletion();
    }

    private void saveSimOperations(ActionEvent event) {
        try {
            getLogicSim().saveSim(getSimSelected());
            clearFormSim();
            if (lbl_InformStatusSim.getText() == "A new sim field is created!") {
                showCreateMessageFxmlIntoStage(event);
            }
            }catch (CapstoneException ex){
                lbl_InformStatusSim.setText(lbl_InformStatusSim.getText() + ex.getMessage());
            }
    }

    public void saveSuccess() {
        lbl_InformStatusSim.setText("A new sim field is created!");
    }

    public void saveFailed() {
        lbl_InformStatusSim.setText("Save action was not successful!");
    }

    private void showCreateMessageFxmlIntoStage(ActionEvent event) {
        try {
            FXMLLoader pane = loadMessageFxml();
            MessageController controller = pane.getController();
            controller.setMessage("CREATE", "SIM");
            Scene scene = createSceneWithCss(pane);
            showStage(event, scene);
            changeCreateControlsAfterShowingMessageFxml();
        } catch (IOException e) {
            log.error("Couldn't load the message FXML", e);
        }
    }

    private void changeCreateControlsAfterShowingMessageFxml() {
        btn_SaveSim.setVisible(false);
        btn_CancelSim.setText("Home");
    }

    private FXMLLoader loadMessageFxml() throws IOException {
        FXMLLoader pane = new FXMLLoader();
        pane.setLocation(getClass().getResource("/fxml/message.fxml"));
        pane.load();
        return pane;
    }

    private Scene createSceneWithCss(FXMLLoader pane) {
        Parent p = pane.getRoot();
        Scene scene = new Scene(p);
        scene.getStylesheets().add(getClass().getResource("/css/style.css").toExternalForm());
        return scene;
    }

    private void showStage(ActionEvent event, Scene scene) {
        Stage window = new Stage();
        window.setScene(scene);
        window.initModality(Modality.WINDOW_MODAL);
        window.initStyle(StageStyle.TRANSPARENT);
        window.initOwner(((Node) event.getSource()).getScene().getWindow());
        window.show();
    }

    private void setTextFieldValueInSim() {
        getSimSelected().setControlNo(txt_ControlNumberSim.getText());
        if (!(txt_SimSerialNumberSim.getText().isEmpty())){
            getSimSelected().setSimSerialNo(txt_SimSerialNumberSim.getText());}else{
            getSimSelected().setSimSerialNo(null);
        }
        getSimSelected().setAccountNo(txt_AccountNumberSim.getText());
        getSimSelected().setMobileNo(txt_MobileNumberSim.getText());
        getSimSelected().setDateReceived(dateHandlerSim().datePickerToLong(datePickerSim.getValue()));
        getSimSelected().setRemarks(txt_RemarksSim.getText());
    }

    private void setTextFieldValueUpdateInSim(){
        getSimSelected().setControlNo(txt_ControlNumberSim.getText());
        if ((txt_SimSerialNumberSim.getText()!= null)){
            getSimSelected().setSimSerialNo(txt_SimSerialNumberSim.getText());}else{
            getSimSelected().setSimSerialNo(null);
        }
        getSimSelected().setAccountNo(txt_AccountNumberSim.getText());
        getSimSelected().setMobileNo(txt_MobileNumberSim.getText());
        getSimSelected().setDateReceived(dateHandlerSim().datePickerToLong(datePickerSim.getValue()));
        getSimSelected().setRemarks(txt_RemarksSim.getText());
    }


    private void updateSimOperation(ActionEvent event) {
        try {
            getLogicSim().updateSim(getSimSelected());
            clearFormSim();
            if (lbl_InformStatusSim.getText() == "The selected field successfully updated!") {
                showUpdateMessageFxmlIntoStage(event);
            }
        }catch (CapstoneException ex){
            lbl_InformStatusSim.setText(lbl_InformStatusSim.getText() + ex.getMessage());
        }
    }

    public void updateSuccess() {
        lbl_InformStatusSim.setText("The selected field successfully updated!");
    }

    public void updateFailed() { lbl_InformStatusSim.setText("Update action was not successful!");}


    private void showUpdateMessageFxmlIntoStage(ActionEvent event) {
        try {
            FXMLLoader pane = loadMessageFxml();
            MessageController controller = pane.getController();
            controller.setMessage("UPDATE", "SIM");
            Scene scene = createSceneWithCss(pane);
            showStage(event, scene);
            changeUpdateControlsAfterShowingMessageFxml();
        } catch (IOException e) {
            log.error("Couldn't load the message FXML", e);
        }
    }

    private void changeUpdateControlsAfterShowingMessageFxml() {
        btn_SaveUpdatedSim.setVisible(false);
        btn_CancelSim.setText("Home");
    }

    private void changeDeleteControlsForConfirmingDeletion() {
        btn_ConfirmDeleteSim.setVisible(false);
        btn_CancelDeleteSim.setText("Ok");
        if (getSimSelected().isActive()){
            lbl_DeleteSim.setText("The sim field is deactivated!");
        }else{
            lbl_DeleteSim.setText("The sim field is activated!");
        }
    }

    public boolean checkTextFieldIsNotNullSim(){
        boolean controlNumber = ControlValidationUtil.textFieldNotEmpty(txt_ControlNumberSim, lbl_ControlNumberSim, "Control number is required!" );
        boolean accountNumber = ControlValidationUtil.textFieldNotEmpty(txt_AccountNumberSim, lbl_AccountNumberSim, "Account number is required!");
        boolean mobileNumber = ControlValidationUtil.textFieldNotEmpty(txt_MobileNumberSim, lbl_MobileNumberSim, "Mobile number is required!");
        boolean dateReceived = ControlValidationUtil.datePickerNotEmpty(datePickerSim, lbl_DatePickerSim, "Date received is required!");
        if (controlNumber && accountNumber && mobileNumber && dateReceived){
             return true;
         }
         else {
             return  false;
         }
    }

    public void initializeCreateViewSim() {
        txt_ControlNumberSim.setText(getLogicSim().getControlNoSim());
        btn_SaveUpdatedSim.setVisible(false);
        btn_SaveSim.setVisible(true);
        datePickerSim.setEditable(false);
        lbl_WelcomeSim.setText("Create a new sim!");
    }

    public  void initializeUpdateViewSim(){
        initializeUpdateViewControls();
        setTextFieldsValues();
    }

    private void setTextFieldsValues() {
        txt_ControlNumberSim.setText(getSimSelected().getControlNo());
        if ((txt_SimSerialNumberSim.getText()!= null)){
            txt_SimSerialNumberSim.setText(getSimSelected().getSimSerialNo());}
        txt_AccountNumberSim.setText(getSimSelected().getAccountNo());
        txt_MobileNumberSim.setText(getSimSelected().getMobileNo());
        datePickerSim.setValue((getSimSelected().getDateReceiveInLocalDate()));
        txt_RemarksSim.setText(getSimSelected().getRemarks());
    }

    private void initializeUpdateViewControls() {
        btn_SaveSim.setVisible(false);
        btn_SaveUpdatedSim.setVisible(true);
        datePickerSim.setEditable(false);
        lbl_WelcomeSim.setText("Update the selected sim!");
    }

    public void clearFormSim(){
        txt_ControlNumberSim.setText("");
        txt_SimSerialNumberSim.setText("");
        txt_AccountNumberSim.setText("");
        txt_MobileNumberSim.setText("");
        datePickerSim.setValue(null);
        txt_RemarksSim.setText("");
    }

    @FXML
    public void loadParent(ActionEvent event) {
        Stage window = (Stage) btn_CancelSim.getScene().getWindow();
        getMainFxmlLoader().loadParent(event);
        window.close();
    }

    @FXML
    public void loadParent1(ActionEvent event) {
        Stage window = (Stage) btn_CancelDeleteSim.getScene().getWindow();
        getMainFxmlLoader().loadParent(event);
        window.close();
    }

    public void setSim(Sim simSelected) {
        this.simSelected = simSelected;
    }


}
