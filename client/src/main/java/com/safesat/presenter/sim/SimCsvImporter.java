package com.safesat.presenter.sim;

import com.safesat.commons.Sim;
import com.safesat.presenter.DateController;
import com.safesat.webservice.sim.SimService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SimCsvImporter {

    private DateController dateController;
    private SimService simService;


    private SimService getSimService() {
        if (null == simService) {
            simService = new SimService();
        }return simService;
    }

    private DateController dateHandler(){
        if (dateController == null){
            dateController= new DateController();
        }return  dateController;
    }

    public void insertCSVFileInTable(String fileDest) throws IOException {
        Reader reader = Files.newBufferedReader(Paths.get(fileDest));
        CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
        List<Sim> list = new ArrayList<>();
        for (CSVRecord csvRecord : csvParser) {
            Sim sim = getSim(csvRecord);
            list.add(sim);
        }
        getSimService().save(list);
    }

    private Sim getSim(CSVRecord csvRecord) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Sim sim = new Sim();
        sim.setDateReceived(dateHandler().datePickerToLong(LocalDate.parse(csvRecord.get(0), formatter)));
        sim.setRemarks(csvRecord.get(1));
        sim.setAccountNo(csvRecord.get(2));
        sim.setControlNo(csvRecord.get(3));
        sim.setMobileNo(csvRecord.get(4));
        if (!(csvRecord.get(5).isEmpty())){
            sim.setSimSerialNo(csvRecord.get(5));}else{
            sim.setSimSerialNo(null);
        }
        return sim;
    }
}
