package com.safesat.presenter.sim;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.safesat.commons.Sim;
import com.safesat.webservice.sim.SimService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.FileNotFoundException;

public class SimReportGenerator {
    private SimService simService;

    public SimService getSimService(){
        if (simService == null) {
            simService = new SimService();
        } return simService;
    }

    public void reportGenerator(String dest) throws Exception{
        ObservableList<Sim> sim = FXCollections.<Sim>observableArrayList(getSimService().getActive());
        Document document = creatingPdfFile(dest);
        Table tableSim = writingInPDFSim(sim, document);
        document.add(tableSim);
        document.close();
    }

    private Document creatingPdfFile(String fileName) throws FileNotFoundException {
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(fileName));
        return new Document(pdfDoc);
    }

    private Table writingInPDFSim(ObservableList<Sim> sim, Document document) {
        addingHeadingForPDFSat(document, "Active records of Sim table");
        Table tableSim = drawingTableInPDFSim();
        creatingSimColumnsForSimReport(tableSim);
        creatingSimRowsForSimReport(sim, tableSim);
        return tableSim;
    }

    private void creatingSimRowsForSimReport(ObservableList<Sim> sim, Table tableSim) {
        for (int i= 0; i<= sim.size()-1; i++){
                tableSim.addCell(new Cell().add(new Paragraph(sim.get(i).getControlNo())));
                if (sim.get(i).getSimSerialNo() != null){
                tableSim.addCell(new Cell().add(new Paragraph(sim.get(i).getSimSerialNo())));}else{
                    tableSim.addCell(new Cell().add(new Paragraph("")));
                }
                tableSim.addCell(new Cell().add(new Paragraph(sim.get(i).getAccountNo())));
                tableSim.addCell(new Cell().add(new Paragraph(sim.get(i).getMobileNo())));
            tableSim.addCell(new Cell().add(new Paragraph(sim.get(i).getDateReceiveInLocalDate().toString())));
            tableSim.addCell(new Cell().add(new Paragraph(sim.get(i).getRemarks())));
            }
    }


    private void addingHeadingForPDFSat(Document document, String message) {
        Paragraph paragraph = new Paragraph(message);
        paragraph.setTextAlignment(TextAlignment.CENTER);
        document.add(paragraph);
    }

    private void creatingSimColumnsForSimReport(Table tableSim) {
        tableSim.addCell(new Cell().add(new Paragraph("Control Number")));
        tableSim.addCell(new Cell().add(new Paragraph("Serial Number")));
        tableSim.addCell(new Cell().add(new Paragraph("Account Number")));
        tableSim.addCell(new Cell().add(new Paragraph("Mobile Number")));
        tableSim.addCell(new Cell().add(new Paragraph("Date Received")));
        tableSim.addCell(new Cell().add(new Paragraph("Remarks")));
    }

    private Table drawingTableInPDFSim() {
        Table tableSim = new Table(UnitValue.createPercentArray(new float[]{30f,30f,30f,30f,30f,30f}));
        tableSim.setFontSize(9);
        tableSim.setItalic();
        tableSim.setTextAlignment(TextAlignment.CENTER);
        return tableSim;
    }
}
