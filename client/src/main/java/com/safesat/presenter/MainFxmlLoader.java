package com.safesat.presenter;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class MainFxmlLoader {

    private final Logger log = LoggerFactory.getLogger(MainFxmlLoader.class);

    public void loadParent(ActionEvent event) {

        try {
            Parent pane = FXMLLoader.load(getClass().getResource("/fxml/main.fxml"));
            Scene scene = new Scene(pane);
            scene.getStylesheets().add(getClass().getResource("/css/style.css").toExternalForm());
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(scene);
            centerStage(window);
            window.setMaximized(true);
            window.show();
        } catch (IOException e) {
            log.error("Couldn't load the home FXML", e);
        }
    }

    private void centerStage(Stage stage) {
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);
    }
}
