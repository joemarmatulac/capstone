package com.safesat.presenter;

import com.safesat.businesslogic.MainControllerBusinessLogic;
import com.safesat.presenter.sat.SatController;
import com.safesat.presenter.sat.SatCsvImporter;
import com.safesat.presenter.sat.SatReportGenerator;
import com.safesat.presenter.sim.SimController;
import com.safesat.commons.SatBox;
import com.safesat.commons.Sim;
import com.safesat.presenter.sim.SimCsvImporter;
import com.safesat.presenter.sim.SimReportGenerator;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.*;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML TableView<Sim> table;
    @FXML private TableView<SatBox> tableSat;
    @FXML private TextField text;
    @FXML private TextField textSat;
    @FXML private Label selecting;
    @FXML private Label selectingSat;
    @FXML private Button btn_DeleteSim;
    @FXML private Button btn_DeleteSat;
    private Sim simSelected;
    private SatBox satSelected;
    private MainControllerBusinessLogic mainControllerBusinessLogic;
    private SimCsvImporter simCsvImporter;
    private SatCsvImporter satCsvImporter;
    private SimReportGenerator simReportGenerator;
    private SatReportGenerator satReportGenerator;
    private final Logger log = LoggerFactory.getLogger(MainController.class);
    ObservableList<Sim> sims;
    ObservableList<SatBox> satBoxs;


    public ObservableList<SatBox> getSatBoxs(){
        if (satBoxs == null){
            satBoxs = FXCollections.observableArrayList();
        }
        return satBoxs;
    }

    public ObservableList<Sim> getSims() {
        if (null== sims){
            sims= FXCollections.observableArrayList();
        }
        return sims;
    }

    private SatReportGenerator getSatReportGenerator(){
        if (satReportGenerator == null){
            satReportGenerator = new SatReportGenerator();
        }return satReportGenerator;
    }

    private SimReportGenerator getSimReportGenerator(){
        if (simReportGenerator == null){
            simReportGenerator = new SimReportGenerator();
        }return simReportGenerator;
    }

    private SatCsvImporter getSatCsvImporter(){
        if (satCsvImporter == null){
            satCsvImporter = new SatCsvImporter();
        }return satCsvImporter;
    }

    private SimCsvImporter getSimCsvImporter(){
        if (simCsvImporter == null){
            simCsvImporter = new SimCsvImporter();
        }return simCsvImporter;
    }

    private MainControllerBusinessLogic getMainControllerBusinessLogic(){
        if (mainControllerBusinessLogic == null){
            mainControllerBusinessLogic = new MainControllerBusinessLogic();
        }return mainControllerBusinessLogic;
    }

    public SatBox getSatSelected() {
        if (satSelected == null) {
            satSelected = new SatBox();
        }return satSelected;
    }

    public Sim getSimSelected() {
        if (simSelected == null) {
            simSelected = new Sim();
        }return simSelected;
    }

    public void simInitializing(){
        clearLabels();
        table.getColumns().clear();
        table.getColumns().addAll(getActiveStatusColumnSim(), getControlNumberColumnSim(), getSimSerialNumberColumnSim(), getAccountNumberColumnSim(), getMobileNumberColumnSim(), getDateReceivedColumnSim(), getRemarksSim());
        table.setPlaceholder(new Label("No visible columns and/or data exist."));
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.setItems(getSims());
        getSims().setAll(settingTableRows());
    }

    @FXML
    public void loadCreateSim(ActionEvent event) {
        clearLabels();
        try{
            FXMLLoader pane = loadFxml("/fxml/sim/create&UpdateSim.fxml");
            SimController controller = pane.getController();
            controller.setSimSelected(getSimSelected());
            Scene scene = createSceneWithCss(pane);
            showStage(event, scene);
        }catch(IOException e){
            log.error("Couldn't load the createSim FXML", e);
        }
    }

    @FXML
    public void loadUpdateSim(ActionEvent event) {
        if (table.getSelectionModel().getSelectedItem() != null) {
            if (table.getSelectionModel().getSelectedItem().isActive()) {
                Sim simSelected = table.getSelectionModel().getSelectedItem();
                showUpdateSimFxmlIntoStage(event, simSelected);
            }else { selecting.setText("Your selected row is deactivated, you can't update deactivated sim!");}
        }else{
            selecting.setText("First select a row and then press update!");
        }
    }

    @FXML
    public void loadChangeStatusSim(ActionEvent event) {
        clearLabels();
        if (table.getSelectionModel().getSelectedItem() != null) {
            Sim simSelected = table.getSelectionModel().getSelectedItem();
            showDeleteSimFxmlIntoStage(event, simSelected);
        }else{
            selecting.setText("First select a row and then press delete!");
        }
    }


    public void satInitializing(){
        clearLabels();
        tableSat.getColumns().clear();
        tableSat.getColumns().addAll(getActiveStatusSat(), getControlNumberColumnSat(), getImeiColumnSat(), getModelNameColumnSat(), getClientAccountColumnSat(), getVehiclePlateNumberColumnSat(), getDateIssuedColumnSat(), getControlNumberSimColumnSat(), getDateReceivedColumnSat(), getRemarksSat());
        tableSat.setPlaceholder(new Label("No visible columns and/or data exist."));
        tableSat.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableSat.setItems(getSatBoxs());
        getSatBoxs().setAll(settingTableSatRows());
    }

    @FXML
    public void loadCreateSat(ActionEvent event) {
        clearLabels();
        try{
            FXMLLoader pane = loadFxml("/fxml/sat/create&UpdateSat.fxml");
            settingUpSatControllerCreateView(pane);
            Scene scene = createSceneWithCss(pane);
            showStage(event, scene);
        }catch(IOException e){
            log.error("Couldn't load the createSat FXML", e);
        }
    }

    @FXML
    void loadUpdateSat(ActionEvent event) {
        clearLabels();
        if (tableSat.getSelectionModel().getSelectedItem() != null) {
            if (tableSat.getSelectionModel().getSelectedItem().isActive() == true) {
                SatBox satSelected = tableSat.getSelectionModel().getSelectedItem();
                showUpdateSatFxmlIntoStage(event, satSelected);
            } else {
                selectingSat.setText("Your selected row is deactivated, you can't update deactivated sat!");
            }
        }else {
            selectingSat.setText("First select a row and then press update!");
        }
    }

    @FXML
    void loadChangeStatusSat(ActionEvent event) {
        clearLabels();
        if (tableSat.getSelectionModel().getSelectedItem() != null) {
            SatBox satSelected = tableSat.getSelectionModel().getSelectedItem();
            showDeleteSatFxmlIntoStage(event, satSelected);
        }else{
            selectingSat.setText("First select a row and then press delete!");
        }
    }

    private SortedList<Sim> settingTableRows() {
        FilteredList<Sim> filteredData = new FilteredList<>(getListSims());
        SortedList<Sim> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(table.comparatorProperty());
        return sortedData;
    }

    private SortedList<Sim> settingTableRowsByTextFieldText() {
        FilteredList<Sim> filteredData = new FilteredList<>(getListSims());
        filterBasedOnTextFieldText(filteredData);
        SortedList<Sim> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(table.comparatorProperty());
        return sortedData;
    }

    private void filterBasedOnTextFieldText(FilteredList<Sim> filteredData) {
        text.textProperty().addListener((observable, oldValue, newValue) -> {
            checkSimSatesWithTextField(filteredData, newValue);
        });
    }

    private void checkSimSatesWithTextField(FilteredList<Sim> filteredData, String newValue) {
        filteredData.setPredicate(sim -> {
            if (newValue == null || newValue.isEmpty()) {
                return true;                }
            String lowerCaseFilter = newValue.toLowerCase();
            if (sim.getControlNo().toLowerCase().contains(lowerCaseFilter)) {
                return true;
            } else if (sim.getSimSerialNo() != null){
                if(sim.getSimSerialNo().toLowerCase().contains(lowerCaseFilter)) {
                    return true;}
            }else if (sim.getAccountNo() != null){
                if (sim.getAccountNo().toLowerCase().contains(lowerCaseFilter)) {
                    return true;}
            }else if (sim.getMobileNo().toLowerCase().contains(lowerCaseFilter)) {
                return true;
            }
            return false;
        });
    }

    private Scene createSceneWithCss(FXMLLoader pane) {
        Parent p = pane.getRoot();
        Scene scene = new Scene(p);
        scene.getStylesheets().add(getClass().getResource("/css/style.css").toExternalForm());
        return scene;
    }

    private void showStage(ActionEvent event, Scene scene) {
        Stage window = new Stage();
        window.setScene(scene);
        window.initModality(Modality.WINDOW_MODAL);
        window.initStyle(StageStyle.TRANSPARENT);
        window.initOwner(((Node) event.getSource()).getScene().getWindow());
        window.setOnHidden(event1 -> {
            getSims().clear();
            getSims().addAll(settingTableRows());
            getSatBoxs().clear();
            getSatBoxs().addAll(settingTableSatRows());
        });
        window.show();
    }

    private void showUpdateSimFxmlIntoStage(ActionEvent event, Sim simSelected) {
        try{
            FXMLLoader pane = loadFxml("/fxml/sim/create&UpdateSim.fxml");
            SimController controller = pane.getController();
            controller.setSimSelected(simSelected);
            Scene scene = createSceneWithCss(pane);
            showStage(event, scene);
        }catch(IOException e){
            log.error("Couldn't load the updateSim FXML", e);
        }
    }

    public FXMLLoader loadFxml(String url)throws IOException{
        FXMLLoader pane = new FXMLLoader();
        pane.setLocation(getClass().getResource(url));
        pane.load();
        return  pane;
    }

    private void showDeleteSimFxmlIntoStage(ActionEvent event, Sim simSelected) {
        try{
            FXMLLoader pane = loadFxml("/fxml/sim/deleteSim.fxml");
            SimController controller = pane.getController();
            controller.setSim(simSelected);
            Scene scene = createSceneWithCss(pane);
            showStage(event, scene);
        }catch(IOException e){
            log.error("Couldn't load the deleteSim FXML", e);
        }
    }

    public static ObservableList<Sim> getListSims(){
        MainControllerBusinessLogic mainController = new MainControllerBusinessLogic();
        ObservableList<Sim> enumSim = mainController.receiveGetSim();
        return enumSim;
    }

    public static TableColumn<Sim, String> getActiveStatusColumnSim(){
        TableColumn<Sim, String> controlNumberColSim = new TableColumn<>("Status");
        PropertyValueFactory<Sim, String> controlNumberCellValueFactory = new PropertyValueFactory<>("status");
        controlNumberColSim.setCellValueFactory(controlNumberCellValueFactory);
        return controlNumberColSim;
    }

    public static TableColumn<Sim, String> getControlNumberColumnSim(){
        TableColumn<Sim, String> controlNumberColSim = new TableColumn<>("Control number");
        PropertyValueFactory<Sim, String> controlNumberCellValueFactory = new PropertyValueFactory<>("controlNo");
        controlNumberColSim.setCellValueFactory(controlNumberCellValueFactory);
        return controlNumberColSim;
    }

    public static TableColumn<Sim, String> getSimSerialNumberColumnSim(){
        TableColumn<Sim, String> simSerialNumberColSim = new TableColumn<>("Sim serial number");
        PropertyValueFactory<Sim, String> simSerialNumberCellValueFactory = new PropertyValueFactory<>("simSerialNo");
        simSerialNumberColSim.setCellValueFactory(simSerialNumberCellValueFactory);
        return simSerialNumberColSim;
    }

    public static TableColumn<Sim, String> getAccountNumberColumnSim(){
        TableColumn<Sim, String> accountNumberColSim = new TableColumn<>("Account number");
        PropertyValueFactory<Sim, String> accountNumberCellValueFactory = new PropertyValueFactory<>("accountNo");
        accountNumberColSim.setCellValueFactory(accountNumberCellValueFactory);
        return accountNumberColSim;
    }

    public static TableColumn<Sim, String> getMobileNumberColumnSim(){
        TableColumn<Sim, String> mobileNumberColSim = new TableColumn<>("Mobile number");
        PropertyValueFactory<Sim, String> mobileNumberCellValueFactory = new PropertyValueFactory<>("mobileNo");
        mobileNumberColSim.setCellValueFactory(mobileNumberCellValueFactory);
        return mobileNumberColSim;
    }

    public static TableColumn<Sim, String> getDateReceivedColumnSim(){
        TableColumn<Sim, String> dateReceivedColSim = new TableColumn<>("Date received");
        PropertyValueFactory<Sim, String> dateReceivedCellValueFactory = new PropertyValueFactory<>("dateReceiveInLocalDate");
        dateReceivedColSim.setCellValueFactory(dateReceivedCellValueFactory);
        return dateReceivedColSim;
    }

    public static TableColumn<Sim, String> getRemarksSim(){
        TableColumn<Sim, String> remarksColSim = new TableColumn<>("Remarks");
        PropertyValueFactory<Sim, String> remarksCellValueFactory = new PropertyValueFactory<>("remarks");
        remarksColSim.setCellValueFactory(remarksCellValueFactory);
        return remarksColSim;
    }

    private SortedList<SatBox> settingTableSatRows() {
        FilteredList<SatBox> filteredData = new FilteredList<>(getListSats());
        filterSatBasedOnTextFieldText(filteredData);
        SortedList<SatBox> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tableSat.comparatorProperty());
        return sortedData;
    }

    private SortedList<SatBox> settingTableSatRowsByTextFieldText() {
        FilteredList<SatBox> filteredData = new FilteredList<>(getListSats());
        filterSatBasedOnTextFieldText(filteredData);
        SortedList<SatBox> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(tableSat.comparatorProperty());
        return sortedData;
    }

    private void filterSatBasedOnTextFieldText(FilteredList<SatBox> filteredData) {
        textSat.textProperty().addListener((observable, oldValue, newValue) -> {
            checkSimStatesWithTextFieldText(filteredData, newValue);
        });
    }

    private void checkSimStatesWithTextFieldText(FilteredList<SatBox> filteredData, String newValue) {
        filteredData.setPredicate(sat -> {
            if (newValue == null || newValue.isEmpty()) {
                return true;
            }
            if (checkSatStatesWithLowerCaseFilter(newValue, sat)) return true;
            return false;
        });
    }

    private boolean checkSatStatesWithLowerCaseFilter(String newValue, SatBox sat) {
        String lowerCaseFilter = newValue.toLowerCase();
        if (sat.getControlNo().toLowerCase().contains(lowerCaseFilter)) {
            return true;
        } else if (sat.getImei().toLowerCase().contains(lowerCaseFilter)) {
            return true;
        }else if (sat.getModelName().toLowerCase().contains(lowerCaseFilter)) {
            return true;
        }else if (sat.getClientAccount() != null){
            if (sat.getClientAccount().toLowerCase().contains(lowerCaseFilter)) {
            return true;}}
        else if (sat.getVehiclePlateNo() != null){
            if (sat.getVehiclePlateNo().toLowerCase().contains(lowerCaseFilter)) {
            return true;}}
        return false;
    }

    private void settingUpSatControllerCreateView(FXMLLoader pane) {
        SatController controller = pane.getController();
        controller.setComboBox(getListSims());
        controller.setSatSelected(getSatSelected());
    }

    private void showUpdateSatFxmlIntoStage(ActionEvent event, SatBox satSelected) {
        try {
            FXMLLoader pane = loadFxml("/fxml/sat/create&UpdateSat.fxml");
            settingUpSatControllerUpdateView(satSelected, pane);
            Scene scene = createSceneWithCss(pane);
            showStage(event, scene);
        } catch (IOException e) {
            log.error("Couldn't load the updateSat FXML", e);
        }
    }

    private void settingUpSatControllerUpdateView(SatBox satSelected, FXMLLoader pane) {
        SatController controller = pane.getController();
        controller.autoCompleteTypeComboBox();
        controller.setComboBox(getListSims());
        controller.setSatSelected(satSelected);
    }

    private void showDeleteSatFxmlIntoStage(ActionEvent event, SatBox satSelected) {
        try{
            FXMLLoader pane = loadFxml("/fxml/sat/deleteSat.fxml");
            SatController controller = pane.getController();
            controller.setSat(satSelected);
            Scene scene = createSceneWithCss(pane);
            showStage(event, scene);
        }catch(IOException e){
            log.error("Couldn't load the deleteSat FXML", e);
        }
    }

    public static ObservableList<SatBox> getListSats(){
        MainControllerBusinessLogic mainController = new MainControllerBusinessLogic();
        ObservableList<SatBox> enumSat = mainController.receiveGetSat();
        return enumSat;
    }

    public static TableColumn<SatBox, String> getActiveStatusSat(){
        TableColumn<SatBox, String> remarksColSat = new TableColumn<>("status");
        PropertyValueFactory<SatBox, String> remarksCellValueFactory = new PropertyValueFactory<>("status");
        remarksColSat.setCellValueFactory(remarksCellValueFactory);
        return remarksColSat;
    }

    public static TableColumn<SatBox, String> getControlNumberColumnSat(){
        TableColumn<SatBox, String> controlNumberColSat = new TableColumn<>("Control number");
        PropertyValueFactory<SatBox, String> controlNumberCellValueFactory = new PropertyValueFactory<>("controlNo");
        controlNumberColSat.setCellValueFactory(controlNumberCellValueFactory);
        return controlNumberColSat;
    }

    public static TableColumn<SatBox, String> getImeiColumnSat(){
        TableColumn<SatBox, String> imeiColSat = new TableColumn<>("Imei");
        PropertyValueFactory<SatBox, String> imeiCellValueFactory = new PropertyValueFactory<>("imei");
        imeiColSat.setCellValueFactory(imeiCellValueFactory);
        return imeiColSat;
    }

    public static TableColumn<SatBox, String> getModelNameColumnSat(){
        TableColumn<SatBox, String> modelNameColSat = new TableColumn<>("Model name");
        PropertyValueFactory<SatBox, String> modelNameCellValueFactory = new PropertyValueFactory<>("modelName");
        modelNameColSat.setCellValueFactory(modelNameCellValueFactory);
        return modelNameColSat;
    }

    public static TableColumn<SatBox, String> getClientAccountColumnSat(){
        TableColumn<SatBox, String> clientAccountColSat = new TableColumn<>("Client account");
        PropertyValueFactory<SatBox, String> clientAccountCellValueFactory = new PropertyValueFactory<>("clientAccount");
        clientAccountColSat.setCellValueFactory(clientAccountCellValueFactory);
        return clientAccountColSat;
    }

    public static TableColumn<SatBox, String> getVehiclePlateNumberColumnSat(){
        TableColumn<SatBox, String> vehiclePlateNumberColSat = new TableColumn<>("Vehicle plate number");
        PropertyValueFactory<SatBox, String> vehiclePlateNumberCellValueFactory = new PropertyValueFactory<>("vehiclePlateNo");
        vehiclePlateNumberColSat.setCellValueFactory(vehiclePlateNumberCellValueFactory);
        return vehiclePlateNumberColSat;
    }

    public static TableColumn<SatBox, String> getDateIssuedColumnSat(){
        TableColumn<SatBox, String> dateIssuedColSat = new TableColumn<>("Date issued");
        dateIssuedColSat.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SatBox,String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<SatBox, String> param) {
                if(param.getValue().getDateIssued()==null){
                    return null;
                }
                return new SimpleObjectProperty<>(param.getValue().getDateIssueInString());
            }
        });
        return dateIssuedColSat;
    }

    public static TableColumn<SatBox, String> getControlNumberSimColumnSat(){
        TableColumn<SatBox, String> controlNumberSimColSat = new TableColumn<> ("Sim mobile number");
        controlNumberSimColSat.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<SatBox,String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<SatBox, String> param) {
                if(param.getValue().getSim()==null){
                    return null;
                }
                return new SimpleObjectProperty<>(param.getValue().getSim().getMobileNo());
            }
        });
        return  controlNumberSimColSat;
    }

    public static TableColumn<SatBox, String> getDateReceivedColumnSat(){
        TableColumn<SatBox, String> dateReceivedColSat = new TableColumn<>("Date received");
        PropertyValueFactory<SatBox, String> dateReceivedCellValueFactory = new PropertyValueFactory<>("dateReceiveInLocalDate");
        dateReceivedColSat.setCellValueFactory(dateReceivedCellValueFactory);
        return dateReceivedColSat;
    }

    public static TableColumn<SatBox, String> getRemarksSat(){
        TableColumn<SatBox, String> remarksColSat = new TableColumn<>("Remarks");
        PropertyValueFactory<SatBox, String> remarksCellValueFactory = new PropertyValueFactory<>("remarks");
        remarksColSat.setCellValueFactory(remarksCellValueFactory);
        return remarksColSat;
    }

    public void initialize(URL location, ResourceBundle resource){
        simInitializing();
        satInitializing();
        changeDeleteButtonWithSelectedRowIsActiveSim();
        changeDeleteButtonWithSelectedRowIsActiveSat();
        table.setItems(settingTableRowsByTextFieldText());
        tableSat.setItems(settingTableSatRowsByTextFieldText());

    }

    private void changeDeleteButtonWithSelectedRowIsActiveSat() {
        tableSat.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (tableSat.getSelectionModel().getSelectedItem().isActive()){
                    btn_DeleteSat.setText("Deactivate");
                }else if(tableSat.getSelectionModel().getSelectedItem().isActive()==false) {
                    btn_DeleteSat.setText("Activate");
                }
            }
        });
    }

    private void changeDeleteButtonWithSelectedRowIsActiveSim() {
        table.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (table.getSelectionModel().getSelectedItem().isActive()){
                    btn_DeleteSim.setText("Deactivate");
                }else{
                    btn_DeleteSim.setText("Activate");
                }
            }
        });
    }

    @FXML
    void loadActivesRowReportSim(ActionEvent event) throws Exception {
        clearLabels();
        String dest = getMainControllerBusinessLogic().getUri() + "reports/reportSim.pdf";
        getSimReportGenerator().reportGenerator(dest);
        selecting.setText("Printing in pdf is finished!");
    }


    @FXML
    void loadActivesRowReportSat(ActionEvent event) throws Exception {
        clearLabels();
        String dest = getMainControllerBusinessLogic().getUri() + "reports/reportSat.pdf";
        getSatReportGenerator().reportGenerator(dest);
        selectingSat.setText("Printing in pdf is finished!");
    }

    @FXML
    void addCsvToSimTable(ActionEvent event) throws Exception {
        clearLabels();
        FileChooser fc= new FileChooser();
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        File selectedFile= fc.showOpenDialog(null);
        if (selectedFile != null){
            String dest = getMainControllerBusinessLogic().getUri() + "CSVs/" + selectedFile.getName();
            getSimCsvImporter().insertCSVFileInTable(dest);
            getSims().clear();
            getSims().addAll(settingTableRows());
            selecting.setText("Sim.csv Successfully inserted into Sim table");

        }
            else {selecting.setText("File is not choosen");
        }
    }


    @FXML
    void addCsvToSatTable(ActionEvent event) throws Exception {
        clearLabels();
        FileChooser fc= new FileChooser();
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        File selectedFile= fc.showOpenDialog(null);
        if (selectedFile != null){
            String dest = getMainControllerBusinessLogic().getUri()+ "CSVs/" + selectedFile.getName();
            getSatCsvImporter().insertCSVFileInTableSat(dest);

            getSatBoxs().clear();
            getSatBoxs().addAll(settingTableSatRows());
            selectingSat.setText("SatBox.csv Successfully inserted into SatBox table");
        }
        else {selecting.setText("File is not choosen");
        }
    }

    void clearLabels(){
        selecting.setText("");
        selectingSat.setText("");
    }
}