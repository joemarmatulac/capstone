package com.safesat.presenter;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


public class ControlValidationUtil {

    @FXML
    public static boolean textFieldNotEmpty(TextField inputTextField, Label label, String sValidationText){
        if(inputTextField.getText() == null){
            label.setText(sValidationText);
            return false;
        }else{
        if (inputTextField.getText().isEmpty()){
            label.setText(sValidationText);
            return false;
        }
        label.setText("");
        return true;
    }}

    @FXML
    public static boolean textFieldUpdateNotEmpty(TextField inputTextField, Label label, String sValidationText){
        if (inputTextField.getText() == null){
            label.setText(sValidationText);
            return false;
        }
        label.setText("");
        return true;
    }

    @FXML
    public  static  boolean textFieldNumericLength(TextField inputTextField, Label label, String sValidationText, String requiredLength){
        if (!inputTextField.getText().matches("^[0-9]{" + requiredLength + "}")){
            label.setText(sValidationText);
            return false;
        }
        label.setText("");
        return  true;
    }

    @FXML
    public static boolean datePickerNotEmpty(DatePicker inputDatePicker, Label label, String sValidationText) {
        if (inputDatePicker.getValue() == null ){
            label.setText(sValidationText);
            return false;
        }
        label.setText("");
        return true;
    }
}
