package com.safesat.presenter;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

public class MessageController {

    @FXML
    private Label lbl_message;

    @FXML
    private Button btn_ok;

    public void setMessage(String action, String module){

        switch (module){
            case "SIM":
                switch (action){
                    case "CREATE":
                        lbl_message.setText("A new sim field is created!");
                        break;
                    case "UPDATE":
                        lbl_message.setText("The selected field successfully updated!");
                        break;
            }case "SAT":
                switch (action){
                    case "CREATE":
                        lbl_message.setText("A new satBox field is created!");
                        break;
                    case "UPDATE":
                        lbl_message.setText("The selected field successfully updated!");
                        break;
                }
        }
    }

    @FXML
    void loadParent(ActionEvent event) {
       Stage stage= (Stage) btn_ok.getScene().getWindow();
       stage.close();
    }
}
