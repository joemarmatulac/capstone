package com.safesat.presenter.sat;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.safesat.commons.SatBox;
import com.safesat.webservice.sat.SatService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.FileNotFoundException;

public class SatReportGenerator {

    private SatService satService;

    private SatService getSatService(){
        if (satService == null){
            satService = new SatService();
        }return satService;
    }

    public void reportGenerator(String dest) throws Exception{
        ObservableList<SatBox> sat = FXCollections.<SatBox>observableArrayList(getSatService().getActive());
        Document document = creatingPdfFile(dest);
        Table tableSat = writingInPDFSat(sat, document);
        document.add(tableSat);
        document.close();
    }

    private Document creatingPdfFile(String fileName) throws FileNotFoundException {
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(fileName));
        return new Document(pdfDoc);
    }

    private Table writingInPDFSat(ObservableList<SatBox> sat, Document document) {
        addingHeadingForPDFSat(document, "Active records of SatBox table");
        Table tableSat = drawingTableInPDFSat();
        creatingSatColumnsForSatReport(tableSat);
        creatingSatRowsForSatReport(sat, tableSat);
        return tableSat;
    }

    private Table drawingTableInPDFSat() {
        Table tableSat = new Table(UnitValue.createPercentArray(new float[]{5f,5f,5f,5f,5f,5f,5f,5f,5f}));
        tableSat.setItalic();
        tableSat.setTextAlignment(TextAlignment.CENTER);
        tableSat.setFontSize(9);
        return tableSat;
    }

    private void addingHeadingForPDFSat(Document document, String message) {
        Paragraph paragraph = new Paragraph(message);
        paragraph.setTextAlignment(TextAlignment.CENTER);
        document.add(paragraph);
    }

    private void creatingSatRowsForSatReport(ObservableList<SatBox> sat, Table tableSat) {
        for (int index = 0; index < sat.size(); index++) {
                tableSat.addCell(new Cell().add(new Paragraph(sat.get(index).getControlNo())));
                tableSat.addCell(new Cell().add(new Paragraph(sat.get(index).getImei())));
                tableSat.addCell(new Cell().add(new Paragraph(sat.get(index).getModelName())));
                checkingClientAccountIsNull(sat, tableSat, index);
                checkingVehiclePlateNoIsNull(sat, tableSat, index);
                checkingDateIssuedIsNull(sat, tableSat, index);
                tableSat.addCell(new Cell().add(new Paragraph(sat.get(index).getDateReceiveInLocalDate().toString())));
                checkingSimIsNull(sat, tableSat, index);
                tableSat.addCell(new Cell().add(new Paragraph(sat.get(index).getRemarks())));
                }
    }

    private void checkingVehiclePlateNoIsNull(ObservableList<SatBox> sat, Table tableSat, int index) {
        if (sat.get(index).getVehiclePlateNo()!= null) {
            tableSat.addCell(new Cell().add(new Paragraph(sat.get(index).getVehiclePlateNo())));
        } else {
            tableSat.addCell(new Cell().add(new Paragraph("")));
        }
    }

    private void checkingDateIssuedIsNull(ObservableList<SatBox> sat, Table tableSat, int index) {
        if (sat.get(index).getDateIssued()!= null) {
            tableSat.addCell(new Cell().add(new Paragraph(sat.get(index).getDateIssueInString())));
        } else {
            tableSat.addCell(new Cell().add(new Paragraph("")));
        }
    }

    private void checkingSimIsNull(ObservableList<SatBox> sat, Table tableSat, int index) {
        if (sat.get(index).getSim() != null) {
            tableSat.addCell(new Cell().add(new Paragraph(sat.get(index).getSim().getControlNo())));
        } else {
            tableSat.addCell(new Cell().add(new Paragraph("")));
        }
    }

    private void checkingClientAccountIsNull(ObservableList<SatBox> sat, Table tableSat, int index) {
        if (sat.get(index).getClientAccount() != null) {
            tableSat.addCell(new Cell().add(new Paragraph(sat.get(index).getClientAccount())));
        } else {
            tableSat.addCell(new Cell().add(new Paragraph("")));
        }
    }

    private void creatingSatColumnsForSatReport(Table tableSat) {
        tableSat.addCell(new Cell().add(new Paragraph("Control Number")));
        tableSat.addCell(new Cell().add(new Paragraph("Imei")));
        tableSat.addCell(new Cell().add(new Paragraph("Model Name")));
        tableSat.addCell(new Cell().add(new Paragraph("Client Account")));
        tableSat.addCell(new Cell().add(new Paragraph("Vehicle Plate Number")));
        tableSat.addCell(new Cell().add(new Paragraph("Date Issued")));
        tableSat.addCell(new Cell().add(new Paragraph("Date Received")));
        tableSat.addCell(new Cell().add(new Paragraph("Sim's Control Number")));
        tableSat.addCell(new Cell().add(new Paragraph("Remarks")));
    }
}
