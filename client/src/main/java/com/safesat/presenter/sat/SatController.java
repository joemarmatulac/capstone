package com.safesat.presenter.sat;

import com.safesat.businesslogic.sat.SatBusinessLogic;
import com.safesat.commons.CapstoneException;
import com.safesat.commons.SatBox;
import com.safesat.commons.Sim;
import com.safesat.presenter.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class SatController {

    @FXML private Label lbl_WelcomeSat;
    @FXML private TextField txt_ControlNumberSat;
    @FXML private TextField txt_ImeiSat;
    @FXML private TextField txt_ModelNameSat;
    @FXML private TextField txt_ClientAccountSat;
    @FXML private TextField txt_VehiclePlateNumberSat;
    @FXML private TextField txt_RemarksSat;
    @FXML private ComboBox<String> cmb_ControlNumberSimSat;
    @FXML private Label lbl_InformStatusSat;
    @FXML private Label lbl_ImeiErrorSat;
    @FXML private Label lbl_ModelNameErrorSat;
    @FXML private Label lbl_ControlNumberErrorSat;
    @FXML private Button btn_SaveUpdatedSat;
    @FXML private Button btn_ConfirmDeleteSat;
    @FXML private Button btn_CancelDeleteSat;
    @FXML private Label lbl_DeleteSat;
    @FXML private Button btn_SaveSat;
    @FXML private Button btn_CancelSat;
    @FXML private DatePicker datePickerSat;
    @FXML private Label lbl_DatePickerSat;
    @FXML private DatePicker datePicker_DateIssuedSat;
    private ObservableList<Sim> sims;
    private String controlNumberSim;
    private SatBox satSelected;
    private Sim simSelected;
    private DateController dateController;
    private SatBusinessLogic satBusinessLogic;
    private MainFxmlLoader mainFxmlLoader;
    private final Logger log = LoggerFactory.getLogger(SatController.class);


    private MainFxmlLoader getMainFxmlLoader(){
        if (mainFxmlLoader == null){
            mainFxmlLoader= new MainFxmlLoader();
        }return mainFxmlLoader;
    }

    private DateController dateHandlerSat(){
        if (dateController == null){
            dateController= new DateController();
        }return  dateController;
    }

    private SatBusinessLogic logicSat() {
        if (satBusinessLogic == null) {
            satBusinessLogic = new SatBusinessLogic(this);
        }return satBusinessLogic;
    }


    public Sim getSimSelected() {
        return simSelected;
    }

    public void setSimSelected(Sim simSelected) {
        this.simSelected = simSelected;
    }

    public SatBox getSatSelected() {
        if (satSelected == null) {
            satSelected = new SatBox();
        }return satSelected;
    }

    public void setSatSelected(SatBox satSelected) {
        if (satSelected.getId() == null){
            initializeCreateViewSat();
        }else{
        this.satSelected = satSelected;
        initializeUpdateViewSat();}
    }

    @FXML
    public void saveSat(ActionEvent event) throws CapstoneException {
        lbl_InformStatusSat.setText("");
        if (checkTextFieldIsNotNullSat()) {
                 SatBusinessLogic satCreateLogic = logicSat();
                 saveSatOperations(event, satCreateLogic);
        }
    }

    public void isNotUnique(boolean field, String errorText) throws CapstoneException {
        if (!field) {
            throw new CapstoneException(errorText);
        }
    }

    @FXML
    void saveUpdatedSat(ActionEvent event) throws CapstoneException {
        lbl_InformStatusSat.setText("");
        if (checkTextFieldIsNotNullSat()) {
                SatBusinessLogic satUpdateLogic = logicSat();
                updateSatOperation(event, satUpdateLogic);

        }
    }

    @FXML
    void deleteSat(ActionEvent event) {
        SatBusinessLogic satDeleteLogic = logicSat();
        satDeleteLogic.changeStatus(getSatSelected().getId());
        changeDeleteControlsForConfirmingDeletion();
    }

    private void saveSatOperations(ActionEvent event, SatBusinessLogic satCreateLogic) {
        try {
            findSimByComboBoxValue();
            setTextFieldValueInSat();
            satCreateLogic.saveSat(getSatSelected());
            clearFormSat();
            if (lbl_InformStatusSat.getText() == "A new satBox field is created!") {
                showCreateMessageFxmlIntoStage(event);
            }
            }catch (CapstoneException ex){
                lbl_InformStatusSat.setText(lbl_InformStatusSat.getText() + ex.getMessage());
        }
    }

    public void saveSuccess() {
        lbl_InformStatusSat.setText("A new satBox field is created!");
    }

    public void saveFailed() {
        lbl_InformStatusSat.setText("Save action was not successful!");
    }

    private void showCreateMessageFxmlIntoStage(ActionEvent event) {
        try {
            FXMLLoader pane = loadMessageFxml();
            MessageController controller = pane.getController();
            controller.setMessage("CREATE", "SAT");
            Scene scene = createSceneWithCss(pane);
            showStage(event, scene);
            changeCreateControlsAfterShowingMessageFxml();
        } catch (IOException e) {
            log.error("Couldn't load the message FXML", e);
        }
    }

    private void changeCreateControlsAfterShowingMessageFxml() {
        btn_SaveSat.setVisible(false);
        btn_CancelSat.setText("Home");
    }

    private FXMLLoader loadMessageFxml() throws IOException {
        FXMLLoader pane = new FXMLLoader();
        pane.setLocation(getClass().getResource("/fxml/message.fxml"));
        pane.load();
        return pane;
    }

    private void showStage(ActionEvent event, Scene scene) {
        Stage window = new Stage();
        window.setScene(scene);
        window.initModality(Modality.WINDOW_MODAL);
        window.initStyle(StageStyle.TRANSPARENT);
        window.initOwner(((Node) event.getSource()).getScene().getWindow());
        window.show();
    }

    public void setTextFieldValueInSat(){
        getSatSelected().setControlNo(txt_ControlNumberSat.getText());
        getSatSelected().setImei(txt_ImeiSat.getText());
        getSatSelected().setModelName(txt_ModelNameSat.getText());
        if (!(txt_ClientAccountSat.getText().isEmpty())){
        getSatSelected().setClientAccount(txt_ClientAccountSat.getText());}else{
            getSatSelected().setClientAccount(null);
        }
        if (!(txt_VehiclePlateNumberSat.getText().isEmpty())){
            getSatSelected().setVehiclePlateNo(txt_VehiclePlateNumberSat.getText());}else{
            getSatSelected().setVehiclePlateNo(null);
        }
        if ((datePicker_DateIssuedSat.getValue() != null)){
            getSatSelected().setDateIssued(dateHandlerSat().datePickerToLong(datePicker_DateIssuedSat.getValue()));}else{
            getSatSelected().setDateIssued(null);
        }
        getSatSelected().setDateReceived(dateHandlerSat().datePickerToLong(datePickerSat.getValue()));
        getSatSelected().setRemarks(txt_RemarksSat.getText());
    }

    private void setTextFieldValueUpdateInSat(){
        getSatSelected().setControlNo(txt_ControlNumberSat.getText());
        getSatSelected().setImei(txt_ImeiSat.getText());
        getSatSelected().setModelName(txt_ModelNameSat.getText());
        if (!(txt_ClientAccountSat.getText().isEmpty())){
            getSatSelected().setClientAccount(txt_ClientAccountSat.getText());}else{
            getSatSelected().setClientAccount(null);
        }
        if (!(txt_VehiclePlateNumberSat.getText().isEmpty())){
            getSatSelected().setVehiclePlateNo(txt_VehiclePlateNumberSat.getText());}else{
            getSatSelected().setVehiclePlateNo(null);
        }
        if ((datePicker_DateIssuedSat.getValue()!= null)){
            getSatSelected().setDateIssued(dateHandlerSat().datePickerToLong(datePicker_DateIssuedSat.getValue()));}else{
            getSatSelected().setDateIssued(null);
        }
        getSatSelected().setDateReceived(dateHandlerSat().datePickerToLong(datePickerSat.getValue()));
        getSatSelected().setRemarks(txt_RemarksSat.getText());
    }

    private boolean findSimByComboBoxValue() throws CapstoneException {
        if (!(cmb_ControlNumberSimSat.getValue().isEmpty())) {
            String mobileNumber = cmb_ControlNumberSimSat.getValue();
            for (int index = 0; index <= sims.size() - 1; index++) {
                if ((sims.get(index).getMobileNo()).equals(mobileNumber) || cmb_ControlNumberSimSat.getValue() == null) {
                    setSimSelected(sims.get(index));
                    getSatSelected().setSim(getSimSelected());
                    return true;
                }
            }
            isNotUnique(false, "Mobile number of sim is not existed!");
            return false;
        }else{
            return false;
        }
    }

    private boolean findSimByComboBoxValueUpdate() throws CapstoneException {
        if ((cmb_ControlNumberSimSat.getValue() != null) && (!(cmb_ControlNumberSimSat.getValue().isEmpty())) ) {

            String mobileNumber = cmb_ControlNumberSimSat.getValue();
            for (int index = 0; index <= sims.size() - 1; index++) {
                if ((sims.get(index).getMobileNo()).equals(mobileNumber)) {
                    setSimSelected(sims.get(index));
                    getSatSelected().setSim(getSimSelected());
                    return true;
                }
            }
            isNotUnique(false, "Control Number of sim is not existed!");
            return false;
        }else{
            return false;
        }
    }

    private void updateSatOperation(ActionEvent event, SatBusinessLogic satUpdateLogic) {
        try{
            findSimByComboBoxValueUpdate();
            setTextFieldValueUpdateInSat();
            satUpdateLogic.updateSat(getSatSelected());
            clearFormSat();
            lbl_InformStatusSat.setText("The selected field successfully updated!");
            if (lbl_InformStatusSat.getText() == "The selected field successfully updated!") {
                showUpdateMessageFxmlIntoStage(event);
            }
            }catch (CapstoneException ex){
                 addComboBoxItems(sims);
                 lbl_InformStatusSat.setText(lbl_InformStatusSat.getText() + ex.getMessage());

            }
    }

    private void showUpdateMessageFxmlIntoStage(ActionEvent event) {
        try {
            FXMLLoader pane = loadMessageFxml();
            MessageController controller = pane.getController();
            controller.setMessage("UPDATE", "SAT");
            Scene scene = createSceneWithCss(pane);
            showStage(event, scene);
            changeUpdateControlsAfterShowingMessageFxml();
        } catch (IOException e) {
            log.error("Couldn't load the message FXML", e);
        }
    }

    private void changeUpdateControlsAfterShowingMessageFxml() {
        btn_SaveUpdatedSat.setVisible(false);
        btn_CancelSat.setText("Home");
    }

    private Scene createSceneWithCss(FXMLLoader pane) {
        Parent p = pane.getRoot();
        Scene scene = new Scene(p);
        scene.getStylesheets().add(getClass().getResource("/css/style.css").toExternalForm());
        return scene;
    }

    private void changeDeleteControlsForConfirmingDeletion() {
        btn_ConfirmDeleteSat.setVisible(false);
        btn_CancelDeleteSat.setText("Ok");
        if (getSatSelected().isActive()){
            lbl_DeleteSat.setText("The satBox field is deactivated!");
        }else{
            lbl_DeleteSat.setText("The satBox field is activated!");
        }
    }

    public boolean checkTextFieldIsNotNullSat(){
        boolean controlNo = ControlValidationUtil.textFieldNotEmpty(txt_ControlNumberSat, lbl_ControlNumberErrorSat, "Control Number is required!");
        boolean modelName = ControlValidationUtil.textFieldNotEmpty(txt_ModelNameSat, lbl_ModelNameErrorSat, "Model name is required!");
        boolean imeiValid = ControlValidationUtil.textFieldNumericLength(txt_ImeiSat, lbl_ImeiErrorSat, "Please enter 15 characters and characters must be numbers from 0-9!", "15");
        boolean dateReceived = ControlValidationUtil.datePickerNotEmpty(datePickerSat, lbl_DatePickerSat, "Date received is required!");

        if (controlNo && modelName && imeiValid && dateReceived){
            return true;}
        else {
            return  false;}
    }

    public void initializeCreateViewSat(){
        autoCompleteTypeComboBox();
        SatBusinessLogic satController= new SatBusinessLogic();
        txt_ControlNumberSat.setText(satController.getControlNoSat());
        cmb_ControlNumberSimSat.setValue(controlNumberSim);
        btn_SaveUpdatedSat.setVisible(false);
        btn_SaveSat.setVisible(true);
        datePickerSat.setEditable(false);
        datePicker_DateIssuedSat.setEditable(false);
        lbl_WelcomeSat.setText("Create a new satBox!");
    }

    public void initializeUpdateViewSat(){
        initializeUpdateViewControls();
        setTextFieldsValues();
    }

    private void setTextFieldsValues() {
        txt_ControlNumberSat.setText(getSatSelected().getControlNo());
        txt_ImeiSat.setText(getSatSelected().getImei());
        txt_ModelNameSat.setText(getSatSelected().getModelName());
        if (getSatSelected().getClientAccount() != null){
        txt_ClientAccountSat.setText(getSatSelected().getClientAccount());}
        if (getSatSelected().getVehiclePlateNo() != null){
        txt_VehiclePlateNumberSat.setText(getSatSelected().getVehiclePlateNo());}
        if ((getSatSelected().getDateIssued() != null)){
        datePicker_DateIssuedSat.setValue(getSatSelected().getDateIssueInLocalDate());}
        datePickerSat.setValue(getSatSelected().getDateReceiveInLocalDate());
        txt_RemarksSat.setText(getSatSelected().getRemarks());
    }

private void initializeUpdateViewControls() {
        if (getSatSelected().getSim() != null){
        cmb_ControlNumberSimSat.setValue(getSatSelected().getSim().getMobileNo().toString());}
        btn_SaveSat.setVisible(false);
        btn_SaveUpdatedSat.setVisible(true);
        datePickerSat.setEditable(false);
        lbl_WelcomeSat.setText("Update the selected satBox!");
    }

    public void clearFormSat(){
        txt_ControlNumberSat.setText("");
        txt_ImeiSat.setText("");
        txt_ModelNameSat.setText("");
        txt_ClientAccountSat.setText("");
        txt_VehiclePlateNumberSat.setText("");
        datePicker_DateIssuedSat.setValue(null);
        datePickerSat.setValue(null);
        txt_RemarksSat.setText("");
    }

    @FXML
    public void loadParent(ActionEvent event) {
        Stage window = (Stage) btn_CancelSat.getScene().getWindow();
        getMainFxmlLoader().loadParent(event);
        window.close();
    }

    @FXML
    public void loadParent1(ActionEvent event) {
        Stage window = (Stage) btn_CancelDeleteSat.getScene().getWindow();
        getMainFxmlLoader().loadParent(event);
        window.close();
    }


    public void setComboBox(ObservableList<Sim> sims1){
        if (sims1.size()==0){
            cmb_ControlNumberSimSat.setValue("");
            btn_SaveSat.setDisable(true);
            lbl_InformStatusSat.setText("First you should create at least one sim!");
        }else {
        sims= sims1;
                addComboBoxItems(sims1);
        controlNumberSim = sims1.get(0).getMobileNo();
        cmb_ControlNumberSimSat.setVisibleRowCount(5);}
    }

    private void addComboBoxItems(ObservableList<Sim> sims1) {
        for (int m= 0; m <= sims1.size()-1; m++ ) {
            if (sims1.get(m).isActive()== true) {
                cmb_ControlNumberSimSat.getItems().add(sims1.get(m).getMobileNo());
            }
        }
    }

    public  void autoCompleteTypeComboBox(){
        new AutoCompleteComboBoxListener<>(cmb_ControlNumberSimSat);

    }

    public void setSat(SatBox satSelected) {
            this.satSelected = satSelected;
    }

}
