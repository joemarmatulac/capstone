package com.safesat.presenter.sat;

import com.safesat.commons.SatBox;
import com.safesat.presenter.DateController;
import com.safesat.webservice.sat.SatService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SatCsvImporter {
    private DateController dateController;
    private SatService satService;

    private SatService getSatService(){
        if (satService == null){
            satService = new SatService();
        }return satService;
    }

    private DateController dateHandler(){
        if (dateController == null){
            dateController= new DateController();
        }return  dateController;
    }

    public void insertCSVFileInTableSat(String fileDest) throws IOException {
        Reader reader = Files.newBufferedReader(Paths.get(fileDest));
        CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
        List<SatBox> list = new ArrayList<>();
        for (CSVRecord csvRecord : csvParser) {
            SatBox sat = getSat(csvRecord);
            list.add(sat);
        }
        getSatService().save(list);
    }

    private SatBox getSat(CSVRecord csvRecord) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        SatBox sat = new SatBox();
        sat.setDateReceived(dateHandler().datePickerToLong(LocalDate.parse(csvRecord.get(0), formatter)));
        sat.setRemarks(csvRecord.get(1));
        sat.setClientAccount(csvRecord.get(2));
        sat.setControlNo(csvRecord.get(3));
        if (!(csvRecord.get(4).isEmpty())){
            sat.setDateIssued(dateHandler().datePickerToLong(LocalDate.parse(csvRecord.get(4), formatter)));}else{
            sat.setDateIssued(null);
        }
        sat.setImei(csvRecord.get(5));
        sat.setModelName(csvRecord.get(6));
        return sat;
    }
}
