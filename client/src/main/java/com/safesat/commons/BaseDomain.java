package com.safesat.commons;

import com.safesat.presenter.DateController;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
@Data
public class BaseDomain implements Serializable {

    private Long id;
    private String status;
    private boolean active = true;
	private String remarks;

	public String getStatus() {
		if (isActive()){
			status = "Activate";
		}else{
			status = "Deactivate";
		}
		return status;
	}

}
