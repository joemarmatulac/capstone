package com.safesat.commons;

import com.safesat.presenter.DateController;
import lombok.Data;

import java.time.LocalDate;
@Data
public class Sim extends BaseDomain {

    private String controlNo;
    private String simSerialNo;
    private String accountNo;
    private String mobileNo;
    private Long dateReceived;
    private LocalDate dateReceiveInLocalDate;


    public LocalDate getDateReceiveInLocalDate() {
        if (getDateReceived() != null) {
           return dateReceiveInLocalDate = new DateController()
                    .longToDatePicker(getDateReceived());
        }
        return null;
    }

}
