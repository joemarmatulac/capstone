package com.safesat.commons;

import com.safesat.presenter.DateController;
import lombok.Data;

import java.time.LocalDate;
@Data
public class SatBox extends BaseDomain{

    private String controlNo;
    private String imei;
    private String modelName;
    private String clientAccount;
    private String vehiclePlateNo;
    private LocalDate dateIssueInLocalDate;
    private Long dateIssued;
    private Sim sim;
    private Long dateReceived;
    private LocalDate dateReceiveInLocalDate;


    public LocalDate getDateIssueInLocalDate() {
        if (getDateIssued() != null) {
           return dateIssueInLocalDate = new DateController()
                    .longToDatePicker(getDateIssued());
        }
        return null;
    }


    public LocalDate getDateReceiveInLocalDate() {
        if (getDateReceived() != null) {
           return dateReceiveInLocalDate = new DateController()
                    .longToDatePicker(getDateReceived());
        }
        return null;
    }



    public String getDateIssueInString() {
        if(getDateIssued() != null) {
            return new DateController().longToString(getDateIssued());
//            return getDateHandlerSat().longToString(getDateIssued());
        }
        return "";
    }

//    public String getDateIssueInString() {
//        if(getDateIssued() != null) {
//            dateIssueInLocalDate= new DateController().longToString(getDateIssued())
//
//        }
//
//        return dateIssueInLocalDate;
//    }
}

