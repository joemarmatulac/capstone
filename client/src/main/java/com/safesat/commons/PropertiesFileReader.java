package com.safesat.commons;

import com.safesat.presenter.sat.SatController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.util.ResourceBundle;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum PropertiesFileReader {
    INSTANCE;
    private String baseUri;
    private String reportUri;
    private final Logger log = LoggerFactory.getLogger(PropertiesFileReader.class);


    public String getBaseUri() {
        return baseUri;
    }

    public String getReportUri() {
        return reportUri;
    }

    public void loadProperties(String filename) {
        String name = System.getProperty("user.dir") + File.separator + "property" + File.separator + filename;
        Properties prop = new Properties();

        try {
        InputStream input = new FileInputStream(name);
            prop.load(input);
            baseUri = prop.getProperty("server_url");
            reportUri= prop.getProperty("report_url");
            input.close();
        } catch (IOException ex) {
            log.error("property file load", ex);
        }
}}
