package com.safesat.webservice;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class HttpClientHandler {
    private final Logger log = LoggerFactory.getLogger(HttpClientHandler.class);

    public HttpResponse get(String url) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet getRequest = new HttpGet(url);
        return client.execute(getRequest);
    }

    public HttpResponse post(String jsonString, String url){
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);
            generateHeader(httpPost);
            StringEntity entity = new StringEntity(jsonString);
            httpPost.setEntity(entity);
            return client.execute(httpPost);
        } catch (IOException e) {
            log.error("post clienHandler", e);
        }
        return null;
    }

    public HttpResponse put(long id, String url){
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPut httpPut = new HttpPut(url + id);
            generateHeaderPut(httpPut);
            return client.execute(httpPut);
        } catch (IOException e) {
            log.error("put clientHandler", e);
        }
        return null;
    }

    public void delete(long id, String url) throws IOException {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpDelete httpDelete = new HttpDelete(url + id);
            client.execute(httpDelete);
        }
    }

    private void generateHeader(HttpPost httpPost) {
        httpPost.addHeader("content-type", "application/json");
        httpPost.addHeader("Accept","application/json");
    }

    private void generateHeaderPut(HttpPut httpPut) {
        httpPut.addHeader("content-type", "application/json");
        httpPut.addHeader("Accept","application/json");
    }


}
