package com.safesat.webservice.sim;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.safesat.commons.PropertiesFileReader;
import com.safesat.commons.Sim;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import com.safesat.webservice.HttpClientHandler;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

public class SimService {

    private HttpClientHandler handlerSim;
    private  Gson gson;
    private String url= PropertiesFileReader.INSTANCE.getBaseUri();
    private final Logger log = LoggerFactory.getLogger(SimService.class);

    private Gson getGson() {
        if (null == gson) {
            gson = new Gson();
        }return  gson;
    }

    public HttpClientHandler getHandler() {
        if (null == handlerSim) {
            handlerSim = new HttpClientHandler();
        } return handlerSim;
    }

    public Collection<Sim> getSim() {
        HttpResponse response= null;
        try {
            response = getHandler().get(url + "api/sim/all");
        } catch (IOException e) {
            log.error("getSim", e);
        }
        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatusLine().getStatusCode());
        }
        return jsonToObjectSim(response);
    }

    public Collection<Sim> getActive(){
        HttpResponse response= null;
        try {
            response=  getHandler().get(url + "api/sim/active");
        } catch (IOException e) {
            log.error("getActiveSim", e);
        }
        return jsonToObjectSim(response);
    }

    public HttpResponse add(Sim simSelected){
        return getHandler().post(getGson().toJson(simSelected),url + "api/sim/add" );
    }

    public HttpResponse update(Sim simSelected) {
        return getHandler().post(getGson().toJson(simSelected), url + "api/sim/update");
    }

    public HttpResponse changeStatus(long json){
       return getHandler().put(json, url + "api/sim/changeStatus/");
    }

    public String getControlNo(){
        return getControlNo(url + "api/sim/generateControlNo");
    }

    public HttpResponse getValidate(Sim simSelected){
        return getHandler().post(getGson().toJson(simSelected),url + "api/sim/validate" );
    }


    public Collection<Sim> jsonToGson(String json){
        Type type = new TypeToken<Collection<Sim>>() {}.getType();
        return getGson().fromJson(json , type);
    }

    private Collection<Sim> jsonToObjectSim(HttpResponse response){
        String responseBody= "";
        try{
         responseBody= EntityUtils.toString(response.getEntity());
         return  jsonToGson(responseBody);
        }catch (IOException ex){
            log.error("Json to object Sim", ex);
        }return  jsonToGson(responseBody);
    }

    public String getControlNo(String url){
        String content= "";
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet getRequest = new HttpGet(url);
        try{
        HttpResponse response = client.execute(getRequest);
        HttpEntity entity = response.getEntity();
        content = EntityUtils.toString(entity);
    } catch (IOException e) {
            log.error("getControlNoSim execute", e);
    }
        return content;
    }

    public HttpResponse save(List<Sim> list) {
        return getHandler().post(getGson().toJson(list),url + "api/sim/addAll" );

    }
}
