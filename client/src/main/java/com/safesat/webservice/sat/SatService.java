package com.safesat.webservice.sat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.safesat.businesslogic.MainControllerBusinessLogic;
import com.safesat.businesslogic.sat.SatBusinessLogic;
import com.safesat.commons.PropertiesFileReader;
import com.safesat.commons.SatBox;
import com.safesat.commons.Sim;
import com.safesat.webservice.HttpClientHandler;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

public class SatService {
    private HttpClientHandler handlerSat;
    private  Gson gson;
    private String url = PropertiesFileReader.INSTANCE.getBaseUri();
    private final Logger log = LoggerFactory.getLogger(SatService.class);


    private Gson getGson() {
        if (null == gson) {
            gson = new Gson();
        }return  gson;
    }

    private HttpClientHandler getHandler() {
        if (null == handlerSat) {
            handlerSat = new HttpClientHandler();
        }return handlerSat;
    }

    public Collection<SatBox> getSat() {
        HttpResponse response= null;
        try {
            response = getHandler().get(url + "api/satBox/all");
        } catch (IOException e) {
            log.error("getSat", e);
        }
        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatusLine().getStatusCode());
        }
        return jsonToObjectSat(response);
    }

    public Collection<SatBox> getActive(){
        HttpResponse response= null;
        try {
            response=  getHandler().get(url + "api/satBox/active");
        } catch (IOException e) {
            log.error("getActiveSat", e);
        }
        return jsonToObjectSat(response);
    }

    public HttpResponse add(SatBox satSelected){
        return getHandler().post(gsonToString(satSelected), url + "api/satBox/add");
    }


    public HttpResponse update(SatBox satSelected) {
        return getHandler().post(gsonToString(satSelected), url + "api/satBox/update");
    }

    public String gsonToString(SatBox sat) {
        return getGson().toJson(sat);
    }

    public HttpResponse changeStatus(long json){
        return getHandler().put(json, url + "api/satBox/changeStatus/");
    }

    public String getControlNoSat(){
        return getControlNo(url + "api/satBox/generateControlNo");
    }

    public HttpResponse getValidate(SatBox satSelected){
        return getHandler().post(getGson().toJson(satSelected),url + "api/satBox/validate" );
    }


    public Collection<SatBox> jsonToGsonSat(String json){
        Type type = new TypeToken<Collection<SatBox>>() {}.getType();
        Collection<SatBox> enumsSat = getGson().fromJson(json , type);
        return enumsSat;
    }


    private Collection<SatBox> jsonToObjectSat(HttpResponse response) {
        String responseBody= "";
        try {
            responseBody = EntityUtils.toString(response.getEntity());
            return jsonToGsonSat(responseBody);
        } catch (IOException ex) {
            log.error("Json to object Sat", ex);
        }return jsonToGsonSat(responseBody);
    }

    public String getControlNo(String url){
        String content= "";
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet getRequest = new HttpGet(url);
        try{
            HttpResponse response = client.execute(getRequest);
            HttpEntity entity = response.getEntity();
            content = EntityUtils.toString(entity);
        } catch (IOException e) {
            log.error("getControlNoSat execute", e);
        }
        return content;
    }

    public HttpResponse save(List<SatBox> list) {
        return getHandler().post(getGson().toJson(list),url + "api/satBox/addAll" );
    }
}
