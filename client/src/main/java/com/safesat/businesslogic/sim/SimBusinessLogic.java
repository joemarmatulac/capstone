package com.safesat.businesslogic.sim;

import com.google.gson.Gson;
import com.safesat.commons.CapstoneException;
import com.safesat.commons.Response;
import com.safesat.presenter.sim.SimController;
import com.safesat.webservice.sim.SimService;
import com.safesat.commons.Sim;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;


public class SimBusinessLogic {
    private SimService simService;
    private SimController simController;
    private  Gson gson;
    private final Logger log = LoggerFactory.getLogger(SimBusinessLogic.class);


    public SimBusinessLogic(SimController simController) {
        this.simController = simController;
    }

    private Gson getGson() {
        if (null == gson) {
            gson = new Gson();
        }return  gson;
    }

    private SimService getSimService() {
        if (null == simService) {
            simService = new SimService();
        }return simService;
    }

    public String getControlNoSim(){
        return getSimService().getControlNo();
    }

    public void saveSim(Sim sim) throws CapstoneException {
        checkSaveFieldsAreValid(sim);
        HttpResponse response = getSimService().add(sim);
        int responseCode= response.getStatusLine().getStatusCode();
        if (responseCode != 200) {
            simController.saveFailed();

        }else{
            simController.saveSuccess();
        }
    }

    private void checkSaveFieldsAreValid(Sim sim) throws CapstoneException {
        HttpResponse responseUnique = getSimService().getValidate(sim);
        int responseCodeUnique = responseUnique.getStatusLine().getStatusCode();
        if (responseCodeUnique == 424) {
            HttpEntity entity = responseUnique.getEntity();
            try {
                String jsonString = EntityUtils.toString(entity);
                Response responseClass = getGson().fromJson(jsonString, Response.class);
                throw new CapstoneException(responseClass.getMessage());
            } catch (IOException e) {
                log.error("Json to String in saveSim", e);
            }
        }
    }

    public void updateSim(Sim getSimSelected) throws CapstoneException {
        checkUpdateFieldsAreValid(getSimSelected);
        HttpResponse response = getSimService().update(getSimSelected);
        if (response.getStatusLine().getStatusCode() != 200) {
            simController.updateFailed();
        }else{
            simController.updateSuccess();
        }
    }

    private void checkUpdateFieldsAreValid(Sim getSimSelected) throws CapstoneException {
        HttpResponse responseUnique = getSimService().getValidate(getSimSelected);
        int responseCodeUnique = responseUnique.getStatusLine().getStatusCode();
        if (responseCodeUnique == 424) {
            HttpEntity entity = responseUnique.getEntity();
            try {
                String jsonString = EntityUtils.toString(entity);
                Response responseClass = getGson().fromJson(jsonString, Response.class);
                throw new CapstoneException(responseClass.getMessage());
            } catch (IOException e) {
                log.error("Json to String in updateSim", e);
            }
        }
    }

    public void changeStatus(long id){
        getSimService().changeStatus(id);
    }

    public void sendPostSimToTable(Sim sim){
        getSimService().add(sim);    }


}
