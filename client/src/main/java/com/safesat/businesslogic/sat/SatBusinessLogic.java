package com.safesat.businesslogic.sat;

import com.google.gson.Gson;
import com.safesat.commons.CapstoneException;
import com.safesat.commons.Response;
import com.safesat.commons.SatBox;
import com.safesat.presenter.sat.SatController;
import com.safesat.webservice.sat.SatService;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class SatBusinessLogic {
    private SatService satService;
    private SatController satController;
    private Gson gson;
    private final Logger log = LoggerFactory.getLogger(SatBusinessLogic.class);


    public SatBusinessLogic(){}

    public SatBusinessLogic(SatController satController) {
        this.satController = satController;
    }

    private Gson getGson() {
        if (null == gson) {
            gson = new Gson();
        }return  gson;
    }


    private SatService getSatService() {
        if (null == satService) {
            satService = new SatService();
        }return satService;
    }


    public String getControlNoSat(){
        SatService getSats= new SatService();
        return getSats.getControlNoSat();
    }

    public void saveSat(SatBox sat) throws CapstoneException {
        HttpResponse responseUnique =  getSatService().getValidate(sat);
        int responseCodeUnique = responseUnique.getStatusLine().getStatusCode();
        if (responseCodeUnique == 424){
            HttpEntity entity = responseUnique.getEntity();
            try {
                String jsonString = EntityUtils.toString(entity);
                Response responseClass = getGson().fromJson(jsonString , Response.class);
                throw new CapstoneException(responseClass.getMessage());
            } catch (IOException e) {
                log.error("Json to String in saveSat", e);
            }
        }
        HttpResponse response = getSatService().add(sat);
        if (response.getStatusLine().getStatusCode() != 200) {
            satController.saveFailed();
        }else{
            satController.saveSuccess();
        }
    }

    public void updateSat(SatBox getSatSelected) throws CapstoneException {
        HttpResponse responseUnique =  getSatService().getValidate(getSatSelected);
        int responseCodeUnique = responseUnique.getStatusLine().getStatusCode();
        if (responseCodeUnique == 424){
            HttpEntity entity = responseUnique.getEntity();
            try {
                String jsonString = EntityUtils.toString(entity);
                Response responseClass = getGson().fromJson(jsonString , Response.class);
                throw new CapstoneException(responseClass.getMessage());
            } catch (IOException e) {
                log.error("Json to String in updateSat", e);
            }
        }
        HttpResponse response = getSatService().update(getSatSelected);
        if (response.getStatusLine().getStatusCode() != 200) {
            satController.saveFailed();
        }else{
            satController.saveSuccess();
        }
    }

    public  void changeStatus(long id){
        SatService delete = getSatService();
        delete.changeStatus(id);
    }
}
