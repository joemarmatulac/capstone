package com.safesat.businesslogic;

import com.safesat.commons.PropertiesFileReader;
import com.safesat.commons.SatBox;
import com.safesat.commons.Sim;
import com.safesat.webservice.sat.SatService;
import com.safesat.webservice.sim.SimService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Collection;

public class MainControllerBusinessLogic{

    private SimService httpHandlerSim;
    private SatService httpHandlerSat;

    private SimService getHttpHandler() {
        if (null == httpHandlerSim) {
            httpHandlerSim = new SimService();
        }return  httpHandlerSim;
    }

    private SatService getHttpHandlerSat() {
        if (null == httpHandlerSat) {
            httpHandlerSat = new SatService();
        }return  httpHandlerSat;
    }

    public ObservableList<Sim> receiveGetSim(){
        return FXCollections.<Sim>observableArrayList(getSimsToString());
    }


    public ObservableList<SatBox> receiveGetSat(){
        return FXCollections.<SatBox>observableArrayList(getSatsToString());
    }

    private ObservableList<Sim> createCollectionSimFromString(String json) {
         return FXCollections.<Sim>observableArrayList(getHttpHandler().jsonToGson(json));
    }

    private Collection<Sim> getSimsToString() {
        return getHttpHandler().getSim();
    }

    private ObservableList<SatBox> createCollectionSatFromString(String json) {
        return FXCollections.<SatBox>observableArrayList(getHttpHandlerSat().jsonToGsonSat(json));
    }

    private Collection<SatBox> getSatsToString() {
        return getHttpHandlerSat().getSat();
    }

    public String getUri(){
        return PropertiesFileReader.INSTANCE.getReportUri();
    }

}
